<%-- 
    Document   : L_canVenMensualxAnio
    Created on : 12-nov-2013, 18:57:24
    Author     : Julio Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="ch" uri="/struts-jquery-chart-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <link href="../Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="../Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <title>Reporte de Ventas</title>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="../Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <center> <br> <br>
        <s:form action="L_canVenMensualxAnioXML">
            ingrese el año <s:textfield name="anio" label="Año" ></s:textfield>
            <s:submit value="enviar"></s:submit>
        </s:form>
    </center>
    <div id="cont2">
        <h2>Chart Lineas</h2>    
        <ch:chart id="chartPoints" autoResize="true" yaxisColor="white" xaxisColor="white" draggable="true"  >
          <s:iterator value="canVenMensualxAnio">
              <ch:chartData label="enero" points="{ show: true }" lines="{ show: true }" list="points" data="[[0,0],[5,%{enero}]]"/>                    
              <ch:chartData label="febrero" points="{ show: true }" lines="{ show: true }" list="points" data="[[5,%{enero}],[10,%{febrero}]]"/>    
              <ch:chartData label="marzo" points="{ show: true }" lines="{ show: true }" list="points" data="[[10,%{febrero}],[15,%{marzo}]]"/>    
              <ch:chartData label="abril" points="{ show: true }" lines="{ show: true }" list="points" data="[[15,%{marzo}],[20,%{abril}]]"/>
              <ch:chartData label="mayo" points="{ show: true }" lines="{ show: true }" list="points" data="[[20,%{abril}],[25,%{mayo}]]"/>    
              <ch:chartData label="junio" points="{ show: true }" lines="{ show: true }" list="points" data="[[25,%{mayo}],[30,%{junio}]]"/>
              <ch:chartData label="julio" points="{ show: true }" lines="{ show: true }" list="points" data="[[30,%{junio}],[35,%{julio}]]"/>    
              <ch:chartData label="agosto" points="{ show: true }" lines="{ show: true }" list="points" data="[[35,%{julio}],[40,%{agosto}]]"/>
              <ch:chartData label="setiembre" points="{ show: true }" lines="{ show: true }" list="points" data="[[40,%{agosto}],[45,%{setiembre}]]"/>    
              <ch:chartData label="octubre" points="{ show: true }" lines="{ show: true }" list="points" data="[[45,%{setiembre}],[50,%{octubre}]]"/>
              <ch:chartData label="noviembre" points="{ show: true }" lines="{ show: true }" list="points" data="[[50,%{octubre}],[55,%{noviembre}]]"/>    
              <ch:chartData label="diciembre" points="{ show: true }" lines="{ show: true }" list="points" data="[[55,%{noviembre}],[60,%{diciembre}]]"/>
          </s:iterator>
        </ch:chart>
        </div>
    <a href="Reportes.jsp"><div id="btn">Regresar</div></a>
    </body>
</html>
