<%-- 
    Document   : HistorialPedidoDeta
    Created on : 30-nov-2013, 13:44:03
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
   <body>
        <s:actionerror theme="bootstrap"/>
        <s:actionmessage theme="bootstrap"/>
        <s:fielderror theme="bootstrap"/>
        <header>
            <a href="index-2.html" class="logo">
                <img src="img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div><%-- CIERRA DIV DE MENU --%>
   <center>
        <div class="widget-body">
        <table  class="table table-striped table-bordered no-margin" style="width: 60%;">
                <th colspan="10">Listar Historial de Pedidos</th>
                <tr class="info">
                    <td align="center">Id Producto</td>
                    <td align="center">Nombre Producto</td>
                    <td align="center">Cantidad</td>
                    <td align="center">Precio</td>
                    <td align="center">Importe</td>
                </tr>
            <s:iterator value="listarDetallePedido">
                <tr>
                    <td align="center"><s:property value="idProducto"></s:property></td>
                    <td align="center"><s:property value="nomProducto"></s:property></td>
                    <td align="center"><s:property value="cantidad"></s:property></td>
                    <td align="center"><s:property value="precio"></s:property></td>
                    <td align="center"><s:property value="importe"></s:property></td>
                    <td>
                    <s:url id="url" action="detalleProdXML">
                        <s:param name="idPro">
                            <s:property value="idProducto"></s:property>
                        </s:param>
                    </s:url>
                    <s:a href="%{url}" cssClass="fs1" aria-hidden="true" data-icon="" >Ver Detalle Producto</s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        </div>
   </center>
    </body>
</html>
