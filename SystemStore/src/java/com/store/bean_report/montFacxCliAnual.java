package com.store.bean_report;

public class montFacxCliAnual {
    String nomCliente;
    double montFacturado;

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public double getMontFacturado() {
        return montFacturado;
    }

    public void setMontFacturado(double montFacturado) {
        this.montFacturado = montFacturado;
    }
    
}
