package com.store.service;


import com.store.bean_entity.Producto;
import java.util.List;

public interface ProductoInterfaz {
    
    public List ListarProductos() throws Exception;
    public boolean ingresoProducto(Producto producto) throws Exception;
    public Producto BuscarProductoID(int id) throws Exception;
    public boolean ActuaizarProductoDao(Producto producto) throws Exception;
    public boolean EliminarProducto(int id) throws Exception;
    public List BuscarProductoNombre(String cad) throws Exception;
    public List ListadoProductoxCategoria(int id) throws Exception;
    public List ListadoProductoRAN(String nomCate) throws Exception;
    public List ListadoProductoBaja() throws Exception;
}
