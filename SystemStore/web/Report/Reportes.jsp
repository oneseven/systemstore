<%-- 
    Document   : canVenMensualxAnio
    Created on : BARRAS
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="ch" uri="/struts-jquery-chart-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <title>Reporte de Ventas</title>
    </head>
<style type="text/css">
#contenedor {
padding:0 0 0 0;
width:auto;
margin-top:10px;
}

#contenido {
    padding:10px 10px 10px 10px;
    font-family:Arial, Helvetica, sans-serif;
    font-size:12px;
    color:black;
    border-width:5px;
    width:auto;
    display:inline-block;
}
</style>

    <body>
        <header>
            <a href="#" class="logo">
                <img src="../Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
<center>
    <div id="contenedor">
        <div id="contenido">
            <h2>Graficos con charts Estaticos</h2><br>
            <a href="cantCompCliXML">Cantidad de compras por cliente  </a><br><br>
            <a href="montoVendAnualXML">montoVendAnual </a><br><br><br><br><br><br><br><br><br><br>
        </div>
        <div id="contenido">
            <h2>Graficos con charts Dinamicos</h2><br>
            <h3>Graficos Barras</h3>
            <a href="proTopAnual.jsp">proTopAnual </a><br><br>
            <a href="canVenMensualxAnio.jsp" cssStyle="title">Cantidad de venta mensual por año</a><br><br>
            <h3>Graficos Pie</h3>
            <a href="montFacxCliAnual.jsp">montFacxCliAnual </a><br><br>
            <a href="proTopFechas.jsp">proTopFechas 2 </a><br><br>
            <a href="montCatAnual.jsp">montCatAnual </a><br><br>
            <a href="CargaCategoriasReporteXML">proMasVendCat  3 </a><br><br>
            <h3>Graficos Lineas</h3>
            <a href="L_canVenMensualxAnio.jsp"> Grafico de Lineas canVenMensualxAnio</a>
        </div>
        <div id="contenido">
            <h2>Reportes en PDF con iReport</h2><br>
            <a href="Report1.jsp">Ver Reporte de Usuarios</a><br><br>
            <a href="grafico1.jsp">Ver Reporte Graficos</a><br><br>
            <a href="report2.jsp">Ver Reporte de Comentario de Productos</a><br><br><br><br><br><br><br><br><br><br>
        </div>
    </div>
</center>
    </body>
</html>
