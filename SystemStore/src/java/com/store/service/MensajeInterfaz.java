package com.store.service;

import com.store.bean_entity.Comentario;
import java.util.List;

public interface MensajeInterfaz {
    public List ListarComentarioProd(int idpro) throws Exception;
    public boolean ingresoComentario(Comentario comen)throws Exception;
}
