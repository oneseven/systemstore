package com.store.service;


import com.store.bean_entity.Categoria;
import java.util.List;

public interface CategoriaInterfaz {
    public List ListarCategorias() throws Exception;
    public boolean ingresoCategoriaDAO(Categoria categoria) throws Exception;
}
