<%-- 
    Document   : montCatAnual
    Created on : PIE con parametro
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="ch" uri="/struts-jquery-chart-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <link href="../Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="../Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <title>Reporte de Ventas</title>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="../Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <center><br><br>
            <s:form action="montCatAnualXML">
                ingrese el año <s:textfield name="anio" label="Año" ></s:textfield>
                <s:submit value="enviar"></s:submit>
            </s:form>
        </center>
        <div id="cont">
            <h2>Monto anual facturado por categoria</h2>
            <ch:chart id="chartPie"  cssStyle="width: 600px; height: 400px;" autoResize="true" pie="true" pieLabel="true" >
                <s:iterator value="montCatAnual">
                    <ch:chartData id="chardata1" label="%{nomCategoria}"  data="%{totfacturado}" />
                    
                </s:iterator>          
            </ch:chart>
        </div>
    <a href="Reportes.jsp"><div id="btn">Regresar</div></a>
    </body>
</html>
