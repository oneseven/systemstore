CREATE DATABASE  IF NOT EXISTS `bd_venta` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_venta`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: bd_venta
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nomCategoria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Televisores'),(2,'Notebook'),(3,'Dispositivo USB'),(4,'Calzado'),(5,'Polos'),(6,'Juguetes'),(7,'Celulares'),(8,'Tablet'),(9,'Video Juegos');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `dni` int(8) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fecNacimiento` date DEFAULT NULL,
  `direccion` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `fk_cliente_usuario1_idx` (`IdUsuario`),
  CONSTRAINT `fk_cliente_usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,1,'victor andres mariano','vasques alegria',74589658,'145896523','vago.14@hotmail.com','1992-06-20','1970 av bolivar'),(2,2,'jose','mendoza legia',32589657,'84894','cesar.1608@outlook.com','1992-06-20','9833 Mt. Dias Blv.'),(3,3,'vero','villanueva lopez',58745698,'684684','cesar.1608@outlook.com','1988-08-09','7484 Roundtree Drive'),(4,4,'pedro','castillo bazan',654654,'84987489','cesar.1608@outlook.com','1992-06-20','9539 Glenside Dr'),(5,5,'ana maria','carranza',987987,'654654','cesar.1608@outlook.com','1995-05-05','1226 Shoe St.'),(6,6,'manuel','vasquez baigorria',12015596,'684684','cesar.1608@outlook.com','1991-06-23','1399 Firestone Drive'),(7,7,'eduardo','gutierres diaz',23668520,'684684','cesar.1608@outlook.com','1992-06-30','5672 Hale Dr.'),(8,8,'carlos','longobardi lopez',21057520,'6546654','cesar.1608@outlook.com','1993-05-18','6387 Scenic Avenue'),(9,9,'alex','monzon asencio',32521420,'5646546','cesar.1608@outlook.com','1992-09-19','8713 Yosemite Ct.'),(10,10,'peredaaa','fernandeeez',123456,'654654','cesar.1608@outlook.com','1992-10-18','florencia'),(11,11,'Ivan','Valencia Valencia',48523697,'65464','cesar.1608@outlook.com','1992-10-18','250 Race Court'),(12,12,'Fernando','Valencia',78945362,'684684','cesar.1608@outlook.com','1992-10-18','1318 Lasalle Street'),(13,13,'David','11',78541236,'864684','cesar.1608@outlook.com','1992-10-18','8713 Yosemite Ct.'),(14,14,'Deibi','22',85412698,'68468864','cesar.1608@outlook.com','1992-10-18','florencia'),(15,15,'Kevin','Norris',78954126,'9512','cesar.1608@outlook.com','1992-10-18','Trujillo'),(16,16,'Jose','44',5263987,'9897987','cesar.1608@outlook.com','1992-10-18','La Esperanza'),(17,17,'Andres','55',456218368,'65465456','cesar.1608@outlook.com','1992-10-18','Miraflores'),(18,18,'Jhon','Brus',45213698,'654654','cesar.1608@outlook.com','1992-10-18',' lima 346'),(19,19,'Pedro','Hilario',74125698,'654654','cesar.1608@outlook.com','1992-10-18','Junin 57'),(20,20,'angie','lozano',65465644,'5525275',NULL,'1992-10-18','av peru 354'),(21,21,'maria jose','valverde ',87878787,'548787985','cesar.1608@outlook.com','1992-10-18','av peru 520'),(22,27,'henry','mariano',7978978,'97845645','cesar.1608@outlook.com',NULL,'');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentario`
--

DROP TABLE IF EXISTS `comentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentario` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `contenido` text,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idcomentario`),
  KEY `fk_idproducto_idx` (`idProducto`),
  KEY `fk_idUsuario_idx` (`idUsuario`),
  CONSTRAINT `fk_idproducto` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`IdUsuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentario`
--

LOCK TABLES `comentario` WRITE;
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
INSERT INTO `comentario` VALUES (1,1,1,'Es un buen producto','2013-12-01 00:00:00'),(2,1,2,'Es un precio muy elevado','2013-12-01 00:00:00'),(3,1,3,'Regular','2013-12-01 00:00:00'),(4,3,21,'Yo lo adquiri y es muy bueno','2013-12-01 00:00:00'),(5,3,22,'No lo recomiendo, es un mal producto','2013-12-01 00:00:00'),(6,4,23,'Muy Muy Bueno.','2013-12-01 00:00:00'),(7,2,15,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(8,2,16,'Excelente y de buena calidad','2013-12-01 00:00:00'),(9,3,17,'Es muy bueno y a bajo costo','2013-12-01 00:00:00'),(10,3,18,'Bueno pero el precio es muy caro','2013-12-01 00:00:00'),(11,3,19,'Lo recomiendo','2013-12-01 00:00:00'),(12,3,20,'Simplemente bueno','2013-12-01 00:00:00'),(13,1,4,'buenisimo','2013-12-01 00:00:00'),(14,1,5,'Este producto es muy bueno =)','2013-12-01 00:00:00'),(15,1,6,'Mal producto','2013-12-01 00:00:00'),(16,1,7,'Lo e adquirido y es excelete','2013-12-01 00:00:00'),(17,1,8,'No cumple las espectativas','2013-12-01 00:00:00'),(18,1,9,'Excelente producto','2013-12-01 00:00:00'),(19,2,10,'No lo recomiendo','2013-12-01 00:00:00'),(20,2,11,'10 puntos','2013-12-01 00:00:00'),(21,2,12,'es un televisor muy economico y bueno.','2013-12-01 00:00:00'),(22,2,13,'kingston la mejor marca =)','2013-12-01 00:00:00'),(23,4,24,'tiene bluetooth de 4g y me parece impresionante','2013-12-01 00:00:00'),(24,2,14,'su tecnologia en 3D es excelente, lo recomiendo','2013-12-01 00:00:00'),(25,4,25,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(26,4,26,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(27,4,27,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(28,4,28,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(29,5,29,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(30,5,30,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(31,5,31,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(32,5,32,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(33,5,33,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(34,5,34,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(35,6,35,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(36,6,36,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(37,6,37,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(38,6,38,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(39,6,39,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(40,7,40,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(41,7,41,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(42,7,42,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(43,7,43,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(44,8,44,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(45,8,45,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(46,8,46,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(47,8,47,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(48,8,48,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(49,8,49,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(50,8,50,'Excelente, lo recomiendo','2013-12-01 00:00:00'),(51,17,1,'tengo uno y me va bien, posiblemente compre otro igual =)','2013-12-02 00:00:00'),(52,19,1,'el precio es un poco elevado. ','2013-12-02 00:00:00'),(53,15,1,'me gusta mucho este televisor, panasonic es una gran marca','2013-12-02 00:00:00');
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_pagina`
--

DROP TABLE IF EXISTS `detalle_pagina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_pagina` (
  `IdDetalle_Pagina` int(11) NOT NULL AUTO_INCREMENT,
  `IdPagina` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  PRIMARY KEY (`IdDetalle_Pagina`),
  KEY `fk_Detalle_Pagina1_idx` (`IdPagina`),
  KEY `fk_detalle_pagina_usuario1_idx` (`IdUsuario`),
  CONSTRAINT `fk_Detalle_Pagina1` FOREIGN KEY (`IdPagina`) REFERENCES `pagina` (`IdPagina`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_detalle_pagina_usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_pagina`
--

LOCK TABLES `detalle_pagina` WRITE;
/*!40000 ALTER TABLE `detalle_pagina` DISABLE KEYS */;
INSERT INTO `detalle_pagina` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,1),(9,1,2),(10,2,2),(11,3,2),(12,4,2),(13,8,2),(14,1,3),(15,2,3),(16,3,3),(17,4,3),(18,8,3),(19,1,4),(20,4,4),(21,8,4),(22,5,4),(23,3,4),(24,1,5),(25,4,5),(26,8,5),(27,5,5),(28,3,5),(29,1,6),(30,4,6),(31,8,6),(32,5,6),(33,3,6),(34,1,16),(35,4,16),(36,6,16),(37,7,16),(38,5,16),(39,3,16),(40,1,17),(41,4,17),(42,6,17),(43,7,17),(44,5,17),(45,3,17),(46,1,18),(47,4,18),(48,6,18),(49,7,18),(50,5,18),(51,3,18),(52,9,1),(53,1,19),(54,4,19),(55,6,19),(56,7,19),(57,5,19),(58,3,19),(59,1,20),(60,4,20),(61,6,20),(62,7,20),(63,5,20),(64,3,20),(65,1,21),(66,4,21),(67,6,21),(68,7,21),(69,5,21),(70,3,21),(71,1,27),(72,4,27),(73,6,27),(74,7,27),(75,5,27),(76,3,27);
/*!40000 ALTER TABLE `detalle_pagina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_venta`
--

DROP TABLE IF EXISTS `detalle_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `idventa` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `importe` double DEFAULT NULL,
  PRIMARY KEY (`iddetalle_venta`),
  KEY `fk_detalle_venta_producto1_idx` (`idproducto`),
  KEY `fk_detalle_venta_venta1_idx` (`idventa`),
  CONSTRAINT `fk_detalle_venta_producto1` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_venta_venta1` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=574 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_venta`
--

LOCK TABLES `detalle_venta` WRITE;
/*!40000 ALTER TABLE `detalle_venta` DISABLE KEYS */;
INSERT INTO `detalle_venta` VALUES (1,1,23,2,50,100),(2,1,24,1,50,50),(3,1,25,1,50,50),(4,1,26,1,50,50),(5,1,40,1,50,50),(6,1,42,1,50,50),(7,2,10,2,50,100),(8,2,21,1,50,50),(9,2,22,1,50,50),(10,2,42,1,50,50),(11,2,32,1,50,50),(12,3,24,2,50,100),(13,3,23,1,50,50),(14,3,31,1,50,50),(15,3,38,1,50,50),(16,4,39,2,50,100),(17,4,34,2,50,100),(18,4,35,2,50,100),(19,4,29,1,50,50),(20,4,27,1,50,50),(21,4,29,1,50,50),(22,4,26,2,50,100),(23,4,34,1,50,50),(24,5,2,2,50,100),(25,5,2,1,50,50),(26,5,25,1,50,50),(27,5,21,1,50,50),(28,6,4,1,50,50),(29,6,5,2,50,100),(30,6,6,2,50,100),(31,6,9,2,50,100),(32,6,8,1,50,50),(33,6,7,1,50,50),(34,6,22,1,50,50),(35,7,6,1,50,50),(36,7,2,3,50,150),(37,7,26,3,50,150),(38,7,4,2,50,100),(39,7,5,2,100,200),(40,7,9,1,100,100),(41,7,6,2,100,200),(42,8,3,1,100,100),(43,8,2,2,100,200),(44,8,5,3,100,300),(45,8,7,2,100,200),(46,9,5,1,100,100),(47,9,4,2,100,200),(48,9,24,2,100,200),(49,10,1,2,100,200),(50,10,1,1,100,100),(51,10,1,1,100,100),(52,10,1,1,100,100),(53,11,1,1,100,100),(54,11,22,1,100,100),(55,12,3,1,100,100),(56,13,6,2,100,200),(57,14,5,1,100,100),(58,14,8,1,100,100),(59,14,7,2,100,200),(60,14,4,2,100,200),(61,15,5,2,100,200),(62,15,26,1,100,100),(63,15,1,1,100,100),(64,16,3,1,100,100),(65,16,9,1,100,100),(66,16,8,1,100,100),(67,16,5,1,100,100),(68,16,11,1,100,100),(69,16,21,1,100,100),(70,16,12,1,100,100),(71,17,13,1,100,100),(72,17,14,1,100,100),(73,17,15,1,100,100),(74,17,16,1,100,100),(75,17,17,1,100,100),(76,17,18,1,100,100),(77,17,19,1,100,100),(78,17,20,1,100,100),(79,18,21,3,100,300),(80,18,22,3,100,300),(81,18,15,3,100,300),(82,18,17,1,100,100),(83,18,18,1,100,100),(84,18,18,1,100,100),(85,19,17,1,100,100),(86,19,14,1,100,100),(87,19,11,1,100,100),(88,19,10,1,100,100),(89,19,13,2,100,200),(90,19,16,1,100,100),(91,19,19,1,100,100),(92,19,21,1,100,100),(93,19,24,2,100,200),(94,19,25,2,100,200),(95,20,26,1,100,100),(96,20,32,3,100,300),(97,20,40,2,10,200),(98,21,41,2,100,200),(99,21,42,2,100,200),(100,21,43,2,100,200),(101,21,45,1,100,100),(102,22,46,1,100,100),(103,22,47,1,100,100),(104,22,48,1,100,100),(105,22,49,1,100,100),(106,22,49,1,10,100),(107,23,48,1,100,100),(108,23,47,2,100,200),(109,23,21,2,100,200),(110,23,41,2,100,200),(111,23,27,2,100,200),(112,24,30,2,100,200),(113,24,1,2,100,200),(114,24,3,2,100,200),(115,25,6,1,100,100),(116,25,9,1,100,100),(117,25,8,1,100,100),(118,25,4,1,100,100),(119,25,48,1,100,100),(120,25,45,1,100,100),(121,25,42,3,100,300),(122,25,41,2,100,200),(489,26,34,3,100,300),(490,26,45,2,100,220),(491,26,48,3,100,300),(492,27,24,2,100,200),(493,27,25,3,100,300),(494,28,26,2,100,200),(495,28,28,4,100,400),(496,29,27,3,100,300),(497,30,24,2,100,200),(498,31,25,4,100,400),(499,32,26,2,100,200),(500,33,20,3,100,300),(501,34,21,2,100,200),(502,35,21,4,100,400),(503,36,9,2,100,200),(504,37,5,3,100,300),(505,38,8,2,100,200),(506,39,7,4,100,400),(507,40,5,2,100,100),(508,41,9,4,100,400),(509,42,6,2,100,200),(510,43,5,4,100,400),(511,44,4,2,100,200),(512,45,7,4,100,400),(513,46,8,3,100,300),(514,47,6,2,100,200),(515,48,5,4,100,400),(516,49,2,3,100,300),(517,50,22,2,100,200),(518,22,22,2,100,200),(519,22,22,1,100,100),(520,22,22,1,100,100),(521,22,22,1,100,100),(522,50,20,2,100,200),(523,50,20,2,100,200),(524,50,20,1,100,100),(525,50,20,2,100,200),(526,50,20,1,100,100),(527,50,20,1,100,100),(528,51,21,2,20,100),(529,53,4,1,800,800),(530,53,2,1,600,600),(531,54,5,1,800,800),(532,54,7,1,800,800),(533,54,12,1,578,578),(534,55,1,1,500,500),(535,55,3,1,700,700),(536,56,1,1,500,500),(537,56,2,2,600,1200),(538,57,1,1,500,500),(539,57,2,1,600,600),(540,57,9,1,666,666),(541,58,5,1,800,800),(542,59,1,1,500,500),(543,59,5,1,800,800),(544,59,6,2,600,1200),(545,60,2,1,600,600),(546,60,6,1,600,600),(547,60,9,1,666,666),(548,63,1,4,500,2000),(549,63,1,4,500,2000),(550,64,1,1,500,500),(551,64,1,4,500,2000),(552,65,1,1,500,500),(553,65,1,5,500,2500),(554,66,1,3,500,1500),(555,66,1,4,500,2000),(556,66,1,5,500,2500),(557,67,1,3,500,1500),(558,67,1,4,500,2000),(559,67,1,5,500,2500),(560,68,2,5,600,3000),(561,68,27,6,87,522),(562,68,35,3,65,195),(563,69,27,2,87,174),(564,69,29,2,65,130),(565,69,22,1,32,32),(566,69,46,1,87,87),(567,70,49,4,48.9,195.6),(568,70,41,1,54,54),(569,71,16,5,999,4995),(570,71,21,4,1099,4396),(571,71,21,5,1099,5495),(572,71,22,3,32,96),(573,71,51,5,48.9,244.5);
/*!40000 ALTER TABLE `detalle_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagina`
--

DROP TABLE IF EXISTS `pagina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagina` (
  `IdPagina` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(45) NOT NULL,
  `Url` varchar(45) NOT NULL,
  PRIMARY KEY (`IdPagina`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagina`
--

LOCK TABLES `pagina` WRITE;
/*!40000 ALTER TABLE `pagina` DISABLE KEYS */;
INSERT INTO `pagina` VALUES (1,'Catalogo Productos','CatalogoProductoXML'),(2,'CRUD Producto','CargaCategoriasXML'),(3,'Cerrar Sesion','cerrarSesionXML'),(4,'Mis Datos','datosClienteXML'),(5,'Ayuda','AyudaXML'),(6,'Historial Compras','historialXML'),(7,'Ver Carrito','verCarritoXML'),(8,'Reportes','Report/Reportes.jsp'),(9,'CRUD Usuarios','CRUDUsuario.jsp');
/*!40000 ALTER TABLE `pagina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `fecCaduca` date DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idproducto`),
  KEY `fk_producto_categoria1_idx` (`idcategoria`),
  CONSTRAINT `fk_producto_categoria1` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,1,'smart tv 3d 40\"',3,500,'2013-01-14','0','img1.png'),(2,1,'lcd 32\"',5,600,'2014-11-22','1','img2.png'),(3,1,'led 40\" samgung',2,700,'2013-11-29','1','img3.png'),(4,1,'OLED 50\" lg',4,800,'2013-11-29','1','img4.png'),(5,1,'smart tv samgung 50\"',7,800,'2013-11-29','1','img5.png'),(6,1,'sony smart 50\"',8,600,'2013-11-29','1','img1.png'),(7,1,'lg 42\" lcd',2,800,'2013-11-29','1','img2.png'),(8,1,'lcd AOC 40\"',1,500,'2013-11-29','1','img3.png'),(9,1,'LED SAMGUNG 32\"',2,666,'2013-11-29','0','img4.png'),(10,1,'LCD LG 50\"',5,500,'2013-11-29','1','img5.png'),(11,1,'SMART TV 3D AOC 46\"',3,980,'2013-11-29','1','img1.png'),(12,1,'LCD 56\" SAMGUNG',6,578,'2013-11-29','1','img2.png'),(13,1,'LG SMART 3D 28\"',2,546,'2013-11-29','1','img3.png'),(14,1,'LED 45\" PANASONIC',1,999,'2013-11-29','1','img4.png'),(15,1,'PANASONIC 48\" LED',4,600,'2013-11-29','1','img5.png'),(16,2,'Laptop Lenovo IdeaPad B490 Intel Dual Core',5,999,'2013-04-17','1','img43.png'),(17,2,'Laptop Lenovo IdeaPad G575 (20081) AMD Dual Core',2,999,'2013-11-29','1','img44.png'),(18,2,'Laptop Toshiba Satellite B40-ASP4202KL, Intel Dual Core',3,999,'2013-11-29','1','img45.png'),(19,2,'Laptop HP Compaq Presario CQ43-411LA, Intel Celeron',6,1099,'2013-11-29','1','img46.png'),(20,2,'Laptop Lenovo IdeaPad G575(20081) ',8,1099,'2013-11-29','1','img47.png'),(21,3,'Laptop Samsung 300E4X.A04VE, Intel Celeron Dual Core',5,1099,'2013-11-29','1','img48.png'),(22,3,'SONY 8GB',4,32,'2013-11-29','1','img12.png'),(23,3,'SONY 4GB',1,21,'2013-11-29','1','img13.png'),(24,3,'KINGSTON 16GB 2.0',2,54,'2013-11-29','1','img13.png'),(25,4,'MARRON CASUAL N40',6,34,'2013-11-29','1','img14.png'),(26,4,'NEGRO CASUAL N40',5,54,'2013-11-29','1','img15.png'),(27,4,'VESTIR N38',4,87,'2013-11-29','1','img16.png'),(28,4,'VESTIR CASUAL N40',6,98,'2013-11-29','1','img17.png'),(29,5,'ROJO CON DISEÑO ',5,65,'2013-11-29','1','img22.png'),(30,5,'AZUL CON DISEÑO',5,65,'2013-11-29','1','img23.png'),(31,6,'POWER RANGIER',6,87,'2013-11-29','1','img24.png'),(32,6,'GOKU DE COLECCION',4,54,'2013-11-29','1','img25.png'),(33,6,'POKEMON COLECCION',5,21,'2013-11-29','1','img26.png'),(34,6,'NARUTO COLECCION',6,32,'2013-11-29','1','img27.png'),(35,7,'GALAXY S3',5,65,'2013-11-29','1','img32.png'),(36,7,'GALAXY ACE',4,98,'2013-11-29','1','img33.png'),(37,7,'NOKIA ASHED',6,87,'2013-11-29','1','img24.png'),(38,7,'MOTOROLA ',6,54,'2013-11-29','1','img25.png'),(39,7,'IPHONE 4G',5,21,'2013-11-29','1','img25.png'),(40,7,'IPHONE 5G',4,32,'2013-11-29','1','img25.png'),(41,7,'IPHONE5G S',6,54,'2013-11-29','1','img32.png'),(42,8,'AOC 7\"',5,87,'2025-10-10','1','img37.png'),(43,8,'SAMGUNG 12\"',4,65,'2025-10-10','1','img38.png'),(44,8,'SONY 10\"',5,45,'2025-10-10','1','img39.png'),(45,8,'AOC 7\"',6,98,'2025-10-10','1','img40.png'),(46,9,'PES 2014',5,87,'2025-10-10','1','img52.png'),(47,9,'FIFA 2014',4,98,'2025-10-10','1','img53.png'),(48,9,'RESIDENT EVIL 5',2,98,'2025-10-10','0','imgdefecto.png'),(49,9,'NIÑOS 2',3,87,'2025-10-10','1','imgdefecto.png'),(50,8,'san andres 10',5,99.99,'2025-10-10','1','img41.png'),(51,4,'aaa',5,48.9,'2013-08-20','1','img21.png'),(52,6,'naturo juguete',10,15.99,'2013-08-20','1','imgdefecto.png'),(53,6,'barbie',3,29.9,'2015-08-20','1','imgdefecto.png'),(54,4,'ADIDAS ADIZERO 90',13,300,'2013-11-22','1','img15.png'),(55,1,'asd',560,12,'2013-11-29','1','img32.png'),(56,2,'ADIDAS ADIZERO 90',3,12,'2013-11-29','1','img47.png'),(57,8,'IPAD 7 pulgadas',33,2888.99,'2013-11-30','1','img32.png'),(58,1,'TV LED Westinghouse LD-2240, 22\'\'',8,529,'2013-12-01','1','img1.png'),(59,1,'TV LCD Sony BRAVIA BX450 , 46\'\' ',7,2079,'2013-11-30','1','img1.png'),(60,1,'TV LED Westinghouse DW50F1Y1, ',17,2079,'2013-11-30','1','img1.png');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo` (
  `IdTipo` int(11) NOT NULL AUTO_INCREMENT,
  `NombreTipo` varchar(45) NOT NULL,
  PRIMARY KEY (`IdTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` VALUES (1,'MASTER'),(2,'Gerente'),(3,'Cliente');
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `IdTipo` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  PRIMARY KEY (`IdUsuario`),
  KEY `fk_Usuario_Tipo_idx` (`IdTipo`),
  CONSTRAINT `fk_Usuario_Tipo` FOREIGN KEY (`IdTipo`) REFERENCES `tipo` (`IdTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'andre','111'),(2,1,'jose','222'),(3,1,'vero','333'),(4,2,'pedro','444'),(5,2,'juan','555'),(6,2,'manuel','666'),(7,2,'edu','777'),(8,2,'carlos','888'),(9,2,'alex','999'),(10,3,'pereda','123'),(11,3,'loopezcesar','123'),(12,3,'ivan','333'),(13,3,'ivan','333'),(14,3,'eduard','222'),(15,3,'zzz','zzz'),(16,3,'hhh','hhh'),(17,3,'ddd','ddd'),(18,3,'rrr','rrr'),(19,3,'uuu','uuu'),(20,3,'ivan070','ivan'),(21,3,'lelele','lelele'),(27,3,'henry','henry');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER menuPaginaUsuario after INSERT ON usuario
  FOR EACH ROW BEGIN
    INSERT INTO detalle_pagina (idpagina, idusuario) values(1,(select max(idusuario) from usuario));
    INSERT INTO detalle_pagina (idpagina, idusuario) values(4,(select max(idusuario) from usuario));
	INSERT INTO detalle_pagina (idpagina, idusuario) values(6,(select max(idusuario) from usuario));
    INSERT INTO detalle_pagina (idpagina, idusuario) values(7,(select max(idusuario) from usuario));
	INSERT INTO detalle_pagina (idpagina, idusuario) values(5,(select max(idusuario) from usuario));
    INSERT INTO detalle_pagina (idpagina, idusuario) values(3,(select max(idusuario) from usuario));
	INSERT INTO cliente (idusuario, nombre, apellidos) values((select max(idusuario) from usuario),'def1','def2');
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `fechaCompra` date DEFAULT NULL,
  `fecEnvio` date DEFAULT NULL,
  `obeservacion` varchar(100) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `montoTotal` double DEFAULT NULL,
  PRIMARY KEY (`idventa`),
  KEY `fk_venta_cliente1_idx1` (`idcliente`),
  CONSTRAINT `fk_venta_cliente1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,1,'2013-01-05',NULL,NULL,'1',654),(2,1,'2013-01-13',NULL,NULL,'1',987),(3,1,'2013-02-20',NULL,NULL,'1',321),(4,1,'2013-02-22',NULL,NULL,'1',654),(5,1,'2013-03-10',NULL,NULL,'1',987),(6,1,'2013-04-05',NULL,NULL,'1',654),(7,1,'2013-04-20',NULL,NULL,'1',321),(8,1,'2013-04-28',NULL,NULL,'1',654),(9,2,'2013-05-09',NULL,NULL,'1',987),(10,2,'2013-05-15',NULL,NULL,'1',654),(11,2,'2013-05-23',NULL,NULL,'1',321),(12,2,'2013-05-28',NULL,NULL,'1',654),(13,2,'2013-06-10',NULL,NULL,'1',987),(14,2,'2013-07-02',NULL,NULL,'1',654),(15,3,'2013-07-09',NULL,NULL,'1',321),(16,3,'2013-07-09',NULL,NULL,'1',654),(17,3,'2013-07-15',NULL,NULL,'1',987),(18,3,'2013-08-02',NULL,NULL,'1',651),(19,3,'2013-08-12',NULL,NULL,'1',354),(20,4,'2013-09-12',NULL,NULL,'1',321),(21,4,'2013-10-19',NULL,NULL,'1',987),(22,5,'2013-10-27',NULL,NULL,'1',684),(23,5,'2013-11-09',NULL,NULL,'1',321),(24,5,'2013-12-02',NULL,NULL,'1',654),(25,5,'2013-12-05',NULL,NULL,'1',987),(26,6,'2013-12-09',NULL,NULL,'1',654),(27,6,'2013-12-10',NULL,NULL,'1',321),(28,6,'2013-12-15',NULL,NULL,'1',654),(29,7,'2012-10-25',NULL,NULL,'1',987),(30,7,'2012-01-03',NULL,NULL,'1',654),(31,7,'2012-01-15',NULL,NULL,'1',321),(32,8,'2012-02-05',NULL,NULL,'1',654),(33,8,'2012-02-10',NULL,NULL,'1',987),(34,8,'2012-02-18',NULL,NULL,'1',654),(35,9,'2012-02-25',NULL,NULL,'1',321),(36,9,'2012-03-08',NULL,NULL,'1',987),(37,9,'2012-03-15',NULL,NULL,'1',654),(38,9,'2012-03-20',NULL,NULL,'1',321),(39,7,'2012-03-21',NULL,NULL,'1',654),(40,7,'2012-03-21',NULL,NULL,'1',987),(41,6,'2012-04-02',NULL,NULL,'1',654),(42,6,'2012-04-10',NULL,NULL,'1',321),(43,8,'2012-04-11',NULL,NULL,'1',654),(44,8,'2012-05-06',NULL,NULL,'1',987),(45,8,'2012-05-14',NULL,NULL,'1',654),(46,6,'2012-06-04',NULL,NULL,'1',321),(47,5,'2012-07-10',NULL,NULL,'1',654),(48,5,'2012-07-12',NULL,NULL,'1',987),(49,4,'2012-08-15',NULL,NULL,'1',654),(50,4,'2012-09-10',NULL,NULL,'1',321),(51,6,'2012-05-16','2012-05-18',NULL,'1',7890),(52,6,'2013-11-29',NULL,NULL,'1',500),(53,6,'2013-11-29',NULL,NULL,'1',1400),(54,6,'2013-11-29',NULL,NULL,'1',2178),(55,2,'2013-11-29',NULL,NULL,'1',1200),(56,2,'2013-11-29',NULL,NULL,'1',1700),(57,2,'2013-11-29',NULL,NULL,'1',1766),(58,2,'2013-11-29',NULL,NULL,'1',800),(59,3,'2013-11-29',NULL,NULL,'1',2500),(60,3,'2013-11-29',NULL,NULL,'1',1866),(62,8,'2013-11-29','2013-12-02',NULL,'1',4888),(63,8,'2013-11-30','2013-12-03',NULL,'1',4000),(64,8,'2013-11-30','2013-12-03',NULL,'1',2500),(65,1,'2013-12-01','2013-12-04',NULL,'1',3000),(66,1,'2013-12-01','2013-12-04',NULL,'1',6000),(67,1,'2013-12-01','2013-12-04',NULL,'1',6000),(68,1,'2013-12-01','2013-12-04',NULL,'1',3717),(69,1,'2013-12-01','2013-12-04',NULL,'1',423),(70,21,'2013-12-01','2013-12-04',NULL,'1',249.6),(71,22,'2013-12-03','2013-12-06',NULL,'1',15226.5);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bd_venta'
--

--
-- Dumping routines for database 'bd_venta'
--
/*!50003 DROP PROCEDURE IF EXISTS `pa_ListaPagina` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_ListaPagina`(in tipo int)
BEGIN
SELECT P.IdPagina, P.Titulo, P.Url
FROM Pagina P, detalle_pagina dp, usuario u 
where P.IdPagina = dp.IdPagina
AND dp.idusuario = u.idusuario
and u.idtipo = tipo
group by p.idpagina;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pa_ValidarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_ValidarUsuario`(usu varchar(45), cla varchar(45))
BEGIN
SELECT concat(c.nombre," ",c.apellidos) as "nombre completo", IdTipo, u.idusuario
FROM Usuario u, cliente c
WHERE u.usuario = usu and u.Clave = cla
and c.idusuario = u.idusuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Actualizar_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Actualizar_producto`(
IN p_idproducto int(11), 
IN p_idcategoria int(11), 
IN p_nombre varchar(45), 
IN p_stock int(11), 
IN p_precio varchar(45), 
IN p_fecCaduca varchar(45), 
IN p_estado varchar(45)
)
BEGIN
UPDATE `producto` 
SET `idcategoria` = p_idcategoria, `nombre` = p_nombre, `stock` = p_stock, `precio` = p_precio, 
`fecCaduca` = p_fecCaduca, `estado` = p_estado
WHERE `idproducto` = p_idproducto;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_addComentario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_addComentario`(in idusu int,  idpro int, com text
)
BEGIN
insert into comentario(idUsuario,idProducto,contenido,fecha)
values(idusu,idpro,com,curdate());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_Producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_Producto`(in 
idcat int, nom varchar(45), sto int, 
pre double, fec date, est varchar(45))
BEGIN
insert into producto(idcategoria,nombre,stock,precio,feccaduca,estado)
values(idcat,nom,sto,pre,fec,est);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Borrar_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Borrar_producto`(IN p_idproducto int(11))
BEGIN
UPDATE `producto`set `estado` = "0"
WHERE `idproducto` = p_idproducto;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_HistorialDetallePedido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_HistorialDetallePedido`(in idped int)
BEGIN
select dv.idproducto, p.nombre, dv.cantidad, dv.precio, dv.importe
from detalle_venta dv, producto p
where dv.idproducto = p.idproducto
and dv.idventa = idped
order by dv.idproducto;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_IngresoDetalleVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_IngresoDetalleVenta`(in idve int, idpro int, can int, pre double, imp double)
BEGIN
insert into detalle_venta(idventa, idproducto,cantidad,precio,importe)
values(idve,idpro,can,pre,imp);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ingresoVentaCabe` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ingresoVentaCabe`(in 
idcli int,
mon double,
est varchar(45))
BEGIN
insert into venta(idcliente,fechacompra,fecEnvio,montoTotal,estado)
values(idcli,curdate(),DATE_ADD(CURDATE(), INTERVAL 3 DAY),mon,est);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insertCliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertCliente`(in idusu int, nom varchar(50), ape varchar(50), dn int,
cel varchar(50), ema varchar(50), fec date, dir varchar(50))
BEGIN
insert into cliente(idusuario,nombre,apellidos,dni,celular,email,fecnacimiento,direccion)
values(idusu,nom,ape,dn,cel,ema,fec,dir);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listaHisorialPedido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listaHisorialPedido`(in idcli int)
BEGIN
select v.idventa, v.fechaCompra, v.fecEnvio, montoTotal
from venta v , cliente c
where v.idcliente = c.idcliente
and c.idcliente = idcli
order by v.idventa desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listarUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listarUsuarios`()
BEGIN
select u.IdUsuario, t.NombreTipo, u.usuario, c.nombre, c.apellidos
from usuario u, tipo t, cliente c
where u.idusuario = c.IdUsuario
and t.IdTipo = u.IdTipo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Listar_categoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Listar_categoria`()
BEGIN
select * from categoria;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listar_clientePorUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_clientePorUsuario`(in idusu int)
BEGIN
select c.idcliente,c.IdUsuario,c.nombre,c.apellidos,c.dni,c.celular,c.email,c.fecNacimiento,c.direccion
 from cliente c, usuario u 
where c.IdUsuario = u.IdUsuario
and u.IdUsuario = idusu;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listar_comentario_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_comentario_producto`(in idpro int)
BEGIN
select concat(nombre, " ", apellidos) as "nombre completo", contenido ,fecha
from comentario com, usuario u, cliente cli
where com.idproducto = idpro
and u.idusuario = com.idusuario
and u.idusuario = cli.idusuario
ORDER BY com.idcomentario DESC
LIMIT 4;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listar_ProdRand` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_ProdRand`(in idcat int)
BEGIN
SELECT p.idproducto, c.nomcategoria, nombre,stock,precio,feccaduca, estado
FROM producto p , categoria c
where p.idcategoria = idcat
and c.idcategoria = p.idcategoria
ORDER BY RAND() LIMIT 3;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Listar_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Listar_producto`()
BEGIN
SELECT `idproducto`, c.nomcategoria, `nombre`, `stock`, 
`precio`, `fecCaduca`, `estado`, imagen
FROM `producto` p, categoria c
where c.idcategoria = p.idcategoria
and estado = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Listar_producto_Por_Categoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Listar_producto_Por_Categoria`(in id int)
BEGIN
select idProducto, nomcategoria, nombre, stock, estado, precio, fecCaduca
from producto p, categoria c
where c.idcategoria = p.idcategoria
and c.idcategoria = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_lista_Prod_Baja` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lista_Prod_Baja`()
BEGIN
SELECT `idproducto`, c.nomcategoria, `nombre`, `stock`, 
`precio`, `fecCaduca`, `estado`, imagen
FROM `producto` p, categoria c
where c.idcategoria = p.idcategoria
and estado = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_nuevoUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_nuevoUsuario`(in  tip int, usu varchar(45), cla varchar(45))
BEGIN
insert into usuario(idtipo,usuario,clave) values (tip,usu,cla);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_producto_Por_Codigo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_producto_Por_Codigo`(
IN p_idproducto int(11))
BEGIN
SELECT `idproducto`, `idcategoria`, `nombre`, `stock`, 
`precio`, `fecCaduca`, `estado`, imagen
FROM `producto`
WHERE `idproducto` = p_idproducto;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_producto_Por_Nombre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_producto_Por_Nombre`(in des varchar(50))
BEGIN
declare pal varchar(250);
set pal = concat('%',trim(des),'%');
select idProducto, nomCategoria,nombre, stock, estado,precio,fecCaduca, imagen
from producto p, categoria c 
where p.idCategoria=c.idCategoria and p.nombre like pal;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Registrar_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Registrar_producto`(
in idcat int(11),
in nom varchar(45),
in st int(11),
in pre double,
in est char(1),
in img varchar(100))
BEGIN
insert into producto(idcategoria,nombre,stock,precio,fecCaduca,estado,imagen)
values (idcat,nom,st,pre,CURDATE(),est,img);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_cantCompCli` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_cantCompCli`()
BEGIN
SELECT concat(c.nombre, "  ",c.apellidos) as 'nombre completo', count(c.idcliente)
from cliente c, venta v
where c.idCliente = v.idcliente
group by c.idCliente order by c.nombre;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_cantVentCliMesAnio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_cantVentCliMesAnio`(in idc int, anio int)
BEGIN
select
(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=1) as 'enero',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=2) as 'febreo',

(select count(venta.fechaCompra)from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=3) as 'marzo',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=4) as 'abril',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=5) as 'mayo',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=6) as 'junio',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=7) as 'julio',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=8) as 'agosto',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=9) as 'setiembre',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=10) as 'octubre',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=11) as 'noviembre',

(select count(venta.fechaCompra) from venta , cliente
where cliente.idcliente = idc and year(venta.fechaCompra)=anio and month(venta.fechaCompra)=12) as 'diciembre';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Rep_canVenMensualxAnio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Rep_canVenMensualxAnio`(in anio int)
BEGIN
select 
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 1 ) as 'enero',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 2) as 'febrero',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 3) as 'marzo',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 4) as 'abril',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 5) as 'mayo',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 6) as 'junio',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 7) as 'julio',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 8) as 'agosto',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 9) as 'septiembre',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 10) as 'octubre',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 11) as 'noviembre',
(select count(venta.fechaCompra) from venta where year(venta.fechaCompra)=anio and month(venta.fechaCompra) = 12) as 'diciembre';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Rep_montCatAnual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Rep_montCatAnual`(in anio int)
BEGIN
/*Total Facturado agrupados por categorías para un año especifico*/
select c.nomcategoria,sum(dt.cantidad*p.precio) as Total_Facturado
from categoria c join producto p
on c.idCategoria=p.idCategoria join
detalle_venta dt on p.idProducto=dt.idProducto
join venta v on dt.idVenta=v.idVenta
where year(v.fechaCompra)=anio
group by c.nomcategoria;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_montFacxCliAnual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_montFacxCliAnual`(in anio int)
BEGIN
/* total facturado por cliente por año*/
select concat(c.nombre," ",c.apellidos) as "nombre compledo", sum(dv.cantidad*p.precio) as "Total Facturdo"
from cliente c, detalle_venta dv, venta v, producto p
where c.idcliente = v.idcliente
and dv.idproducto = p.idproducto
and v.idventa = dv.idventa
and year(v.fechaCompra)=anio
group by c.nombre, year(v.fechaCompra)
order by c.nombre;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Rep_montoVendAnual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Rep_montoVendAnual`()
BEGIN
select year(v.fechaCompra) as anio, sum(p.precio*dt.cantidad)
from detalle_venta dt, venta v, producto p
where dt.idVenta=v.idVenta
and dt.idProducto = p.idProducto
group by year(v.fechaCompra);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_proMasVendCat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_proMasVendCat`(in idcat int, inicio date, fin date)
BEGIN
Select  p.nombre , count(dv.idproducto) as c
FROM producto p ,detalle_venta dv, categoria c, venta v
where dv.idProducto = p.idProducto
and v.idventa = dv.idventa
and p.idcategoria = c.idcategoria
and c.idcategoria = idcat
and v.fechaCompra BETWEEN inicio AND fin
group by p.nombre
order by c desc
limit 5 ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_proTopAnual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_proTopAnual`(in anio int)
BEGIN
SELECT  p.nombre, count( dv.idproducto) c
FROM detalle_venta dv, venta v, producto p
WHERE year(v.fechaCompra) = anio
and dv.idventa = v.idventa
and p.idproducto = dv.idproducto
group by p.nombre
ORDER BY c DESC
LIMIT 5;
/*SELECT idProducto, count( idProducto ) c
FROM detalle_venta p, venta v
WHERE v.fechaCompra
BETWEEN "2013-01-01"
AND "2013-12-31"
GROUP BY idProducto
ORDER BY c DESC
LIMIT 5 ;
*/
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rep_proTopFechas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rep_proTopFechas`(in inicio date, fin date)
BEGIN
SELECT  p.nombre, count( dv.idproducto) c
FROM detalle_venta dv, venta v, producto p
WHERE v.fechaCompra BETWEEN inicio AND fin
and dv.idventa = v.idventa
and p.idproducto = dv.idproducto
group by p.nombre
ORDER BY c DESC
LIMIT 5;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_search_ProdxNomCate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_search_ProdxNomCate`(in nomcate varchar(45))
BEGIN
select idProducto, nomcategoria, nombre, stock, estado, precio, fecCaduca,imagen
from producto p, categoria c
where c.idcategoria = p.idcategoria
and c.nomCategoria = nomcate
ORDER BY RAND() LIMIT 3;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UltimoIdVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_UltimoIdVenta`()
BEGIN
select max(idventa) from venta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_updateCliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateCliente`(in 
idcli int, 
nom varchar(45), 
ape varchar(45), 
dn int, 
cel varchar(45),
ema varchar(45), 
fec date, 
dir varchar(45))
BEGIN
update cliente
set nombre = nom,
apellidos = ape,
dni = dn,
celular = cel,
email = ema,
fecNacimiento = fec,
direccion = dir
where idcliente = idcli;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-03 16:57:48
