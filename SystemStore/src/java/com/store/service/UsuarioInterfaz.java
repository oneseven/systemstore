package com.store.service;

import com.store.bean_entity.Cliente;
import com.store.bean_menu.Usuario;
import java.util.List;


public interface UsuarioInterfaz {
    public Usuario ValidarUsuario(String User, String Clave) throws Exception;
    public List ListarPagina(int Tipo) throws Exception;
    public Cliente listarDatosCliente(int idusuario) throws Exception;
    public boolean nuevoUsuario(String User, String Clave) throws Exception;
    public boolean insertCliente(Cliente cliente) throws Exception;
    public boolean updateCliente(Cliente cliente) throws Exception;
    public List ListarUsuarios() throws Exception;
}
