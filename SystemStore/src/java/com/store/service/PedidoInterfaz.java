package com.store.service;


import com.store.bean_entity.DetallePedido;
import com.store.bean_entity.Pedido;
import java.util.List;

public interface PedidoInterfaz {
    public boolean IngresoPedido(Pedido p, List<DetallePedido>ListaPedido)throws Exception;
    public List ListarPedidos(int idcli) throws Exception;
    public List ListarDetallePedidos(int idven) throws Exception;
}
