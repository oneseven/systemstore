<%-- 
    Document   : detalle_Prod
    Created on : 17-nov-2013, 22:19:17
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
    <body><center>
        <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <div class="dashboard-wrapper">
            <div class="left-sidebar">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="widget">
                      <div class="widget-header">
                        <div class="title">
                            <a id="lineChart"><s:property   value="%{Producto.nombre}"></s:property></a>
                        </div>
                        <span class="tools">
                          <a class="fs1" aria-hidden="true" data-icon=""></a>
                        </span>
                      </div>
                      <div class="widget-body">
                        <div id="line_chart">
                            
                            <form action="agregarCarritoXML">
                                <img src="imgProducto/<s:property value="%{Producto.imagen}" ></s:property>" alt=""><br>
                            Codigo: <s:a value="idPro" href="#"> <s:property   value="%{Producto.idProducto}"></s:property></s:a><br>
                            
                            Nombre: <s:property   value="%{Producto.nombre}"></s:property><br>
                            Stock: <s:property   value="%{Producto.stock}"></s:property><br>
                            Precio: <s:property   value="%{Producto.precio}"></s:property><br>
                            <s:textfield placeholder="cantidad" name="cant" ></s:textfield><br>
                            
                            
                            
                            <s:hidden name="idPro" value="%{Producto.idProducto}"></s:hidden>
                            <s:hidden name="nomCate" value="%{Producto.Categoria.nomCategoria}"></s:hidden>
                            <s:hidden name="pre" value="%{Producto.precio}"></s:hidden>
                            <s:hidden name="nomPro" value="%{Producto.nombre}"></s:hidden>
                            <s:submit  name="btnagregar" value="agregar" cssClass="btn btn-info" align="center"></s:submit>
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                        
                        <%--
                  <div class="span6">
                  <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="columnChart">Comentarios Referenciales</a>
                    </div>
                    <span class="tools">
                      <a class="fs1" aria-hidden="true" data-icon=""></a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div id="column_chart">
                    <s:iterator value="listarComentProd">
                        <blockquote>
                      <span class="title">
                        <s:property value="usuario.usuario"></s:property>
                      </span>
                      <p>
                        <s:property value="contenido"></s:property>
                      </p>
                    </blockquote>
                      </s:iterator>
                    </div> column_chart 
                  </div>
                  </div>--%>
                  
                  
                  <div class="span12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="accordion" data-original-title="">Comentarios Referenciales </a>
                    </div>
                    <span class="tools">
                      <a class="fs1" aria-hidden="true" data-icon="" data-original-title=""></a>
                    </span>
                  </div>
                  <% int cont=0;
                     String cad="";
                  %>
                  <div class="widget-body">
                    <div id="accordion1" class="accordion no-margin">
                        <s:iterator value="listarComentProd">
                            <% cont++; 
                            if(cont==1){
                                 cad = "collapseOne";
                            }else if(cont==2){
                                cad = "collapseTwo";
                            }else
                                cad = "collapseThree";
                            
                            %>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                            <a href="#<% out.print(cad); %>" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-user">
                            </i>
                            <s:property value="usuario.usuario"></s:property> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fecha:<s:property value="fecha"></s:property>
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="<% out.print(cad); %>" style="height: 0px;">
                          <div class="accordion-inner">
                            <s:property value="contenido"></s:property>
                          </div>
                        </div>
                      </div>
                          </s:iterator>
                    </div>
                  </div>
                  
                </div>
              </div>
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  <div class="row-fluid">
              <div class="span8">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="fluidContact" data-original-title="">Ingrese su comentario</a>
                    </div>
                    <span class="tools">
                      <a class="fs1" aria-hidden="true" data-icon="" data-original-title=""></a>
                    </span>
                  </div>
                    <div class="widget-body" class="span12" >
                      <s:form action="IngresoComentarioXML"cssClass="no-margin">
                      
                          <s:hidden name="idPro" value="%{Producto.idProducto}"></s:hidden>
                          <s:textarea  rows="3" id="description3" name="contenido" cssClass="input-block-level" placeholder="Description"/>
                          <s:submit type="submit" cssClass="btn btn-info pull-right" value="comentaaaar"></s:submit>
                      
                    </s:form>
                  </div>
                </div>
              </div>
            </div>
                  
                  </div>
                  
                </div>
            </div>
                  
                  
                  
              
                  
                  
        </div>
    <center><h3>Productos relacionados </h3></center>
        <div class="container wrapper">
            <div class="row" style="margin-top: 30px; width: 85%;">
                <div class="span12">
                    <ul class="thumbnails portfolio ">
                        <s:iterator value="listarProductosRAN" >
                            <li class="span3 cat-2" style="margin-top: 30px; width: 300px;">
                                <div class="thumbnail">
                                    <a class="js-fancybox" rel="album" href="assets/sinImagen.gif">
                                        <img src="imgProducto/<s:property value="imagen"></s:property>" alt="">
                                    </a>
                                    <b><a>Nombre Producto: </a></b> <s:property value="nombre"></s:property><br>
                                    <b><a>Categoria: </a></b><s:property value="Categoria.nomCategoria"></s:property><br>
                                    <b><a>Precio: </a></b> <s:property value="precio"></s:property><br>
                                    <s:url id="url" action="detalleProdXML">
                                    <s:param name="idPro">
                                        <s:property value="idProducto"></s:property>
                                    </s:param>
                                    <s:param name="nomCate">
                                        <s:property value="Categoria.nomCategoria"></s:property>
                                    </s:param>
                                </s:url>
                                <s:a cssClass="btn btn-success bottom-margin" href="%{url}" >Ver Detalles</s:a>
                                </div>
                            </li>
                        </s:iterator>
                    </ul>
                </div>
            </div>
        </div><br><br><br>
    </center>
    </body>
</html>