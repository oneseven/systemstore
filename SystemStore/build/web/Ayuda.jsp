<%-- 
    Document   : Ayuda
    Created on : 03-dic-2013, 2:29:15
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <div class="span12">
                <div class="widget no-margin">
                  <div class="widget-header">
                    <div class="title">
                      Help
                    </div>
                    <span class="tools">
                      <a class="fs1" aria-hidden="true" data-icon="" data-original-title=""></a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div id="accordion1" class="accordion no-margin">
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseOne" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Anim pariatur cliche reprehenderit?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseOne" style="height: 0px;">
                          <div class="accordion-inner">
                            Dkateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat s Brunch 3 wolf moon tempor Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat.
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseTwo" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Food truck quinoa nesciunt laborum eiusmod?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseThree" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Food truck quinoa nesciunt laborum eiusmod?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseFour" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Brunch 3 wolf moon tempor Question?
                          </a>
                        </div>
                        <div class="accordion-body in collapse" id="collapseFour" style="height: auto;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseFive" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Enim eiusmod high life accusamus terry richardson ad squid?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseSix" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseSeven" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Good truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseSeven" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseEight" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Officia aute, non cupidatat skateboard dolor brunch?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseEight" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseNine" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseNine" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseTen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseTen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseEleven" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseEleven" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseTwelve" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Food truck quinoa nesciunt laborum eiusmod?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseTwelve" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseThirteen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Brunch 3 wolf moon tempor Question?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseThirteen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseFourteen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Enim eiusmod high life accusamus terry richardson ad squid?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseFourteen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseFifteen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Food truck quinoa nesciunt laborum eiusmod?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseFifteen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseSixteen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Brunch 3 wolf moon tempor Question?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseSixteen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a href="#collapseSeventeen" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                            <i class="icon-info-sign">
                            </i>
                            Enim eiusmod high life accusamus terry richardson ad squid?
                          </a>
                        </div>
                        <div class="accordion-body collapse" id="collapseSeventeen" style="height: 0px;">
                          <div class="accordion-inner">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                          </div>
                        </div>
                      </div>
                      
                      
                    </div>
                  </div>
                </div>
              </div>
    </body>
</html>
