package com.store.action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import com.store.bean_entity.*;
import com.store.bean_menu.*;
import com.store.bean_report.*;
import com.store.dao.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.io.FileUtils;

public class MetodosAction extends ActionSupport {
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //METODOS PARA SESIONES DE USUARIO Y CLIENTE 
    int idUsuario, idCliente;
    String usu;
    String cla;
    public int tip;
    List<Pagina> listarPaginas = null;
    Cliente cliente;
    String nomCli, apeCli, celCli, emailCli, dirCli; // bean cliente
    int dni;
    Date fechNacCli;
    public String DatosCliente(int idUsuario) throws Exception{
        try{
            UsuarioDAO clienteDao =  new UsuarioDAO();
            cliente = clienteDao.listarDatosCliente(idUsuario);
        }
        catch(Exception e){
            System.out.println("error ---------------> DatosCliente"+e.getMessage());
            e.printStackTrace();
        }
        if(cliente.getNombre()==null){
            addActionError("Error al encontrar su informacion de Cliente :( ");
            return ERROR;
        }
        else{
            addActionMessage(" Puede actualizar sus datos de Cliente :) ");
            return SUCCESS;
        }
    }
    public String ListarPaginas(int tip) throws Exception{
        try{
            UsuarioDAO pd = new UsuarioDAO();
            listarPaginas = pd.ListarPagina(tip);
        }catch(Exception e){
            System.out.println("error valida en el action ----> ListarPaginas"+e.getMessage());
        }
        if(listarPaginas.isEmpty()==false){
            addActionMessage("Opciones disponibles para su sesion");
            return SUCCESS;
        }else{
            addActionError("Error al listar sus opciones en el menu");
            return ERROR;
        }
    }
    public String IniciarSesion() throws Exception{
        Usuario usuario = new Usuario();
        UsuarioDAO ud = new UsuarioDAO();
        try{
            usuario = ud.ValidarUsuario(usu, cla);
        }catch(Exception e){
            System.out.println("error valida en el action ----> IniciarSesion"+e.getMessage());
            e.printStackTrace();
        }
        if(usuario.getUsuario()!=null){
            Map<String, Object>sesionUsuario = ActionContext.getContext().getSession();
            sesionUsuario.put("sesionUsuario", usuario);
            if(usuario.getTipo().getIdTipo() == 1){
                        //con esto listo los datos del cliente
                        idUsuario = usuario.getIdUsuario();
                        DatosCliente(idUsuario);
                //pregunto qe tipo de usuario es para listar el menu dinamico
                tip = 1;
                ListarPaginas(tip);
            }else if(usuario.getTipo().getIdTipo() == 2){
                tip = 2;
                ListarPaginas(tip);
            }else{
                tip = 3;
                ListarPaginas(tip);
            }
            addActionMessage("Bienvenido : " + usuario.getUsuario());
            return SUCCESS;
        }else{
            addActionError("Usuario y Clave son Incorrecto");
            return ERROR;
        }
    }
    public String cerrarSesion() {
        Map sessionLogout = ActionContext.getContext().getSession();
        sessionLogout.remove("sesionUsuario");
        addActionMessage("Has salido de su sesion");
        return SUCCESS;
     }
    public String ListarMenuconDatos()throws Exception{
        Map ses = ActionContext.getContext().getSession();
        if(ses.get("sesionUsuario")==null){
            System.out.println("La Session de Usuario no esta Creada");
            addActionMessage("Usted Necesita Iniciar Session Para Ver Nuestros Productos");
            return SUCCESS;
        }
        else{
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            
            int idusu = usu.getIdUsuario();
            DatosCliente(idusu);
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Puede actualizar su informacion personal ");
            return ERROR;
        }
    }
    public String ValidaUsuarioSession()  {//con este metodo me aseguro que el usuario pueda estar con una session iniciada
          //para poder realizar un carrito de compras
          Map ses = ActionContext.getContext().getSession();
          if (ses.get("sesionUsuario")==null) {
              System.out.println("La Session de Usuario no esta Creada");
              addActionMessage("Usted Necesita Iniciar Session Para Ver Nuestros Productos");
              return ERROR;
          } else {
              Usuario u = new Usuario(); // este objeto para la session
              u = (Usuario)ses.get("sesionUsuario");
              addActionMessage("usuario : " + u.getUsuario());
              return SUCCESS;
          }
      }
    public String nuevoUsuario() throws Exception {
        boolean res = false;
        try{
            UsuarioDAO usuarioDao = new UsuarioDAO();
            res = usuarioDao.nuevoUsuario(usu, cla);
        }catch(Exception e){
            System.out.println("error en el action ------> nuevoUsuario"+e.getMessage());
            e.printStackTrace();
        }
        if(res == true){
            addActionMessage("puede iniciar sesion con su nuevo usuario");
            return SUCCESS;
        }else{
             addActionMessage("Error al registrarlo como nuevo usuario, intelo nuevamente");
            return ERROR;
        }
    }
    public String NuevoCliente() throws Exception{
        boolean resultado = false;
        Map ses = ActionContext.getContext().getSession();
        Usuario u = (Usuario) ses.get("sesionUsuario");
        try{
            int iduser = u.getIdUsuario();
            UsuarioDAO user = new UsuarioDAO();
            Cliente cli = new Cliente();
            cli = user.listarDatosCliente(iduser);
            idCliente = cli.getIdCliente();
            if(idCliente == 0){
                //INSERTAR CLIENTE, NO EXISTE EL IDCLIENTE
                Usuario usu= new Usuario();
                usu.setIdUsuario(iduser);
                Cliente clie = new Cliente();
                clie.setUsuario(usu);
                clie.setNombre(nomCli);
                clie.setApellidos(apeCli);
                clie.setDni(dni);
                clie.setCelular(celCli);
                clie.setTelefono(emailCli);
                clie.setFecNacimniento(fechNacCli);
                clie.setDireccion(dirCli);
                resultado = user.insertCliente(clie);
            }else{
                System.out.println("entro al else de nuevo cliente action");
                //ACTUALIZAR CLIENTE PORQUE EXISTE EL IDCLIENTE
                Cliente clie = new Cliente();
                clie.setIdCliente(idCliente);
                clie.setNombre(nomCli);
                clie.setApellidos(apeCli);
                clie.setDni(dni);
                clie.setCelular(celCli);
                clie.setTelefono(emailCli);
                clie.setFecNacimniento(fechNacCli);
                clie.setDireccion(dirCli);
                resultado = user.updateCliente(clie);
            }
        }catch(Exception ex){
            System.out.println("error en el action ----> NuevoCliente " + ex.getMessage());
            ex.printStackTrace();
        }
        if(resultado == true){
            int idtipo = u.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Sus datos fueron actualizados correctamente");
            ListarMenuconDatos();
            return SUCCESS;
        }else{
            addActionError("No se pudieron actualizar sus datos :( ");
            return ERROR;
        }
        
    }
    public String Ayuda() throws Exception {
        try{
            Map ses = ActionContext.getContext().getSession();
            Usuario u = (Usuario) ses.get("sesionUsuario");

            int idtipo = u.getTipo().getIdTipo();
                ListarPaginas(idtipo);
            return SUCCESS;
        }catch(Exception e){
            return ERROR;
        }
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //METODOS PARA EL CRUD DE PRODUCTO
    int idPro, idCat, sto, cant, idPed;
    double pre, acumTotal;
    String nomPro, est, cad, nomCate, contenido;
    Date fec;
    Producto producto;
    List<String> categorias;
    List<Producto> listarProductos = null;
    List<Producto> listaProductoBaja = null;
    List<Producto> listarProductosRAN = null;
    List<Comentario> listarComentProd = null;
    List<Pedido> listarPedidos = null;
    List<DetallePedido> listarDetallePedido = null;
    public String ListaProductos() throws Exception{
        try{
            ProductoDAO productodao = new ProductoDAO();
            listarProductos = productodao.ListarProductos();
            listaProductoBaja = productodao.ListadoProductoBaja();
        }catch(Exception e){
            System.out.println("error ---------------> ListaProductos ");
        }
        if(listarProductos.isEmpty()==false){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            return SUCCESS;
        }else{
            addActionError("error ---------------> ListaProductos");
            return ERROR;
        }
    }
    public String ListaCategorias() throws Exception {
        System.out.println("entro al listarcategorias action");
        categorias = new ArrayList<>();
        CategoriaDAO categoriaDao=new CategoriaDAO();
        System.out.println("trae los resultados del dao");
        categorias=categoriaDao.ListarCategorias();
        ListaProductos();
        return SUCCESS;
    }
    public static List<Categoria> ListarCategoriasCatalogo()throws Exception{
       CategoriaDAO marcaDao = new CategoriaDAO();
       System.out.println("Exito de Listado Marca Action");
        return marcaDao.ListarCategorias();
        
   }
    public String ingresarProducto() throws Exception {
        System.out.println("entro al metodo");
        boolean R = false;
        
        try{
           
            System.out.println("entro al try");
            Categoria cat= new Categoria();
            cat.setIdCategoria(idCat);
            Producto prod = new Producto();
            prod.setCategoria(cat);
            prod.setNombre(nomPro);
            prod.setStock(sto);
            prod.setPrecio(pre);
            prod.setFechaCaduca(fec);
            prod.setEstado(est);
            
            ProductoDAO productoDao = new ProductoDAO();
            R = productoDao.ingresoProducto(prod);
        }catch(Exception e){
            System.out.println("error en el DAO ---->ingresarProducto "+e.getMessage());
            e.printStackTrace();
        }
        ListaCategorias();
        if(R == true){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Producto Registrado Exitosamente");
            ListaProductos();
            return SUCCESS;
        }else{
            addActionError("Producto No Registrado");
            return ERROR;
        }
    }
    public String BuscarProductoID() throws Exception{
        try{
            ProductoDAO productoDao =  new ProductoDAO();
            producto = productoDao.BuscarProductoID(idPro);
            listarProductosRAN = productoDao.ListadoProductoRAN(nomCate);
            listarComentarios(idPro);
        }
        catch(Exception e){
            System.out.println("error ---------------> BuscarProductoID"+e.getMessage());
            e.printStackTrace();
        }
        ListaProductos();
        ListaCategorias();
        if(producto.getNombre()==null){
            addActionError("Producto No Encontrado :( ");
            return ERROR;
        }
        else{
            Map ses = ActionContext.getContext().getSession();
            
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
             ListarPaginas(idtipo);
            addActionMessage("Producto Encontrado :) ");
            return SUCCESS;
        }
    }
    public String listarComentarios(int idpro){
        try{
        MensajeDAO comentarioProductoDao = new MensajeDAO();
        listarComentProd = comentarioProductoDao.ListarComentarioProd(idpro);
        return SUCCESS;
        }
        catch(Exception e){
          System.out.println("error ---------------> ListarComentarioDao"+e.getMessage());
            e.printStackTrace(); 
            return ERROR;
        }
    }
    public String RegistroComentario(){
        boolean R = false;
        Map ses = ActionContext.getContext().getSession();
         Usuario u = (Usuario) ses.get("sesionUsuario");
        try{
            u.getIdUsuario();
            Comentario comen = new Comentario();
            comen.setUsuario(u);
            Producto pro = new Producto();
            pro.setIdProducto(idPro);
            comen.setProducto(pro);
            comen.setContenido(contenido);
            MensajeDAO msjDao = new MensajeDAO();
            R = msjDao.ingresoComentario(comen);
            listarComentarios(idPro);
            ProductoDAO productoDao = new ProductoDAO();
            listarProductosRAN = productoDao.ListadoProductoRAN(nomCate);
            BuscarProductoID();
        }catch(Exception e){
            System.out.println("Error Nuevo Equipo DAO");
        }
        if(R == true){
            addActionMessage("Comentario Registrado");
            contenido="";
            return SUCCESS;
        }else{
            addActionError("Comentario No Registrado");
            return ERROR;
        }
    }
    public String ActualizaProducto()throws Exception{
        boolean R = false;
        try{
            Producto producto = new Producto();
            Categoria categoria = new Categoria();
            categoria.setIdCategoria(idCat);
            producto.setCategoria(categoria);
            producto.setNombre(nomPro);
            producto.setStock(sto);
            producto.setEstado(est);
            producto.setPrecio(pre);
            producto.setFechaCaduca(fec);
            producto.setIdProducto(idPro);
            ProductoDAO productoDao = new ProductoDAO();
            R = productoDao.ActuaizarProductoDao(producto);
        }catch(Exception e){
            System.out.println("error ---------------> ActualizaProducto");
        }
        ListaProductos();
        ListaCategorias();
        if(R == true){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Registro Actualizado Correctamente :) ");
            return SUCCESS;
        }else{
            addActionError("Complete correctamente los campos ");
            return ERROR;
        }
    }
    public String EliminarProducto() throws Exception{
        boolean R = false;
        try{
            ProductoDAO productooDao = new ProductoDAO();
            R = productooDao.EliminarProducto(idPro);
        }catch(Exception e){
            System.out.println("Error Eliminar Equipo ACTION");
        }
        ListaCategorias();
        ListaProductos();
        if(R == true){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Registro Dado de baja satisfactoriamente ");
            return SUCCESS;
        }else{
            addActionError("No se pudo dar de baja al producto ");
            return ERROR;
        }
    }
    public String BuscaProductoNombre()throws Exception{
         try {
            ProductoDAO productoDao = new ProductoDAO();
            listarProductos = productoDao.BuscarProductoNombre(cad);
        } catch (Exception e) {
            System.out.println("error ---------------> EliminarProducto " + e.getMessage());
            e.printStackTrace();
        }
        if (listarProductos.isEmpty()) {
            addActionMessage("No se encontraron resultados.. ");
            return ERROR;
        } else {
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            addActionMessage("Productos que encontrados.. ");
            return SUCCESS;
        }
    }
    public String AgregarCarrito() throws Exception {
       System.out.println("entro al metodo agregar carrito");
        Map ses = ActionContext.getContext().getSession(); // objeto session se usa en para todo
        
        try {
             //aqui solo para validar al usuario
          if (ses.get("sesionUsuario")==null) {
              System.out.println("La Session de Usuario no esta Creada");
              addActionMessage("Usted Necesita Iniciar Session Para Ver Nuestros Productos");
              
          } else {
             System.out.println("else de agregar carrito");
              Usuario u = new Usuario(); // este objeto para la session
              u = (Usuario)ses.get("sesionUsuario");
              int idtipo = u.getTipo().getIdTipo();
              ListarPaginas(idtipo);
              addActionMessage("Usuario : " + u.getUsuario());
          }
            //------------------------------------------------------------------------------
          //aqui para agregar al carrito
            Vector<DetallePedido> Detalle = null;
            if (ses.get("sesDetalle")!=null) {
                Detalle = (Vector<DetallePedido>)ses.get("sesDetalle");
                for (int i = 0; i < Detalle.size(); i++) {
                    acumTotal+=Detalle.get(i).getImporte();
                }
            }else{
                Detalle = new Vector<DetallePedido>();
            }
            if (ses.get("sesDetalle")!=null) {
               DetallePedido dp  =new DetallePedido();
               dp.setIdDetallePedido(Detalle.size()+1);
               dp.setIdProducto(idPro);
               dp.setNomProducto(nomPro);
               dp.setCantidad(cant);
               dp.setPrecio(pre);
               dp.setImporte(pre*cant);
               dp.setTotal((pre*cant)+ acumTotal);
               System.out.println("Mas de una sesion Session");
               System.out.println("Total para Sumar :" + acumTotal);
               System.out.println("Nuevo Total:"+((pre*cant)+acumTotal) );
               acumTotal=0;
               Detalle.add(dp);
            } else {
            DetallePedido dp  =new DetallePedido();
            dp.setIdDetallePedido(Detalle.size()+1);
            dp.setIdProducto(idPro);
            dp.setNomProducto(nomPro);
            dp.setCantidad(cant);
            dp.setPrecio(pre);
            dp.setImporte(pre*cant);
            dp.setTotal(pre*cant);
            System.out.println("Primera Session");
            System.out.println("Primer Total :"+ (pre*cant));
            Detalle.add(dp);
            }
            Map<String,Object> sesionDetalle = ActionContext.getContext().getSession();
            sesionDetalle.put("sesDetalle",Detalle);
            System.out.println("Session Detalle Creada con Exito");
            
            
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("Error Session Detalle");
           return ERROR;
        }
    }
    public String QuitarProductoSession(){
        Map ses = ActionContext.getContext().getSession(); // objeto session se usa en para todo
        Vector<DetallePedido> Detalle = new Vector<DetallePedido>();
        try {
           Vector<DetallePedido> listaProductos=(Vector<DetallePedido>) ses.get("sesDetalle");
           listaProductos.remove(idPro-1);
           for (int i = 0; i < listaProductos.size(); i++) {
            //aqui recupero el nuevo total para disminuir ccuando se quita un producto
                     acumTotal+=listaProductos.get(i).getImporte();
                }
           for (int i = 0; i <listaProductos.size(); i++) {
              DetallePedido dp  =new DetallePedido();
              dp.setIdDetallePedido(Detalle.size()+1);
              dp.setIdProducto(listaProductos.get(i).getIdProducto());
                    //System.out.println(listaProductos.get(i).getIdProducto());
              dp.setNomProducto(listaProductos.get(i).getNomProducto());
                    //System.out.println(listaProductos.get(i).getNomProducto());
              dp.setCantidad(listaProductos.get(i).getCantidad());
                    //System.out.println(listaProductos.get(i).getCantidad());
              dp.setPrecio(listaProductos.get(i).getPrecio());
                    //System.out.println(listaProductos.get(i).getPrecio());
              dp.setImporte(listaProductos.get(i).getCantidad()*listaProductos.get(i).getPrecio());
                     //System.out.println(listaProductos.get(i).getCantidad()*listaProductos.get(i).getPrecio());
              dp.setTotal(acumTotal);
              Detalle.add(dp);
           }      
        Map<String,Object> sesionDetalle = ActionContext.getContext().getSession();
        sesionDetalle.put("sesDetalle",Detalle);
        System.out.println("Se Elimino con Exito el Producto");
        Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
        return SUCCESS;
        } catch (Exception e) {
            System.out.println("Error al Momento de Elminar el Producto");
            return ERROR;
        }
    }
    public static List<Producto> ListarProdPorCate ( int idmar)throws Exception{
        ProductoDAO productodao = new ProductoDAO();
        System.out.println("Exito de ListadoAction"+productodao.ListadoProductoxCategoria(idmar));
        return productodao.ListadoProductoxCategoria(idmar);
    }
    public String IngresoPedido()throws Exception{
        boolean resultado = false;
        Map ses = ActionContext.getContext().getSession();
         Usuario u = (Usuario) ses.get("sesionUsuario");
         List<DetallePedido> ListaPedido=(List<DetallePedido>) ses.get("sesDetalle");
         double totalPedido = ListaPedido.get(ListaPedido.size()-1).getTotal();
         try {
            int iduser=u.getIdUsuario();
            UsuarioDAO user = new UsuarioDAO();
            Cliente c = new Cliente();
            c=user.listarDatosCliente(iduser);
            idCliente=c.getIdCliente();
            Pedido p = new Pedido();
            p.setIdCliente(idCliente);
            p.setMontoTotal(ListaPedido.get(ListaPedido.size()-1).getTotal());
            p.setEstado("1");
            PedidoDAO pedidodao = new PedidoDAO(); 
            resultado = pedidodao.IngresoPedido(p, ListaPedido);
            System.out.println("Pedido Ingresado con Exito ACTION");
            System.out.println("idCliente: "+idCliente);
            System.out.println("Total: "+ListaPedido.get(ListaPedido.size()-1).getTotal());
            EnviarCorreo();
            ListaPedido.clear();
            Map<String,Object> sesionDetalle = ActionContext.getContext().getSession();
            sesionDetalle.put("sesDetalle",ListaPedido);
            
            
         }
         catch(Exception e){
            System.out.println("Error al Momento de Ingresar Pedido ACTION"+e.getMessage());
            e.printStackTrace();
         }
         if (resultado==true) {
             Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
             ListarPaginas(idtipo);
           addActionMessage("Exito Registro PedidoAction");
            return SUCCESS;
        } else {
            addActionError("Error al Momento de Ingresar Vuelva a Intentar mas Tarde");
            return ERROR;
        }
    }
    public String ListarHistorialPedidos() throws Exception{
        try{
            Map ses = ActionContext.getContext().getSession();
            Usuario u = (Usuario) ses.get("sesionUsuario");
            Cliente c = new Cliente();
            UsuarioDAO user = new UsuarioDAO();
            int iduser=u.getIdUsuario();
            c=user.listarDatosCliente(iduser);
            int idcli =c.getIdCliente();
            PedidoDAO pedidoDao = new PedidoDAO();
            listarPedidos = pedidoDao.ListarPedidos(idcli);
        }catch(Exception e){
            System.out.println("error ---------------> ListarHistorialPedidos "+e.getMessage());
            e.printStackTrace();
        }
        if(listarPedidos.isEmpty()==false){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            return SUCCESS;
        }else{
            addActionError("error ---------------> ListarHistorialPedidos");
            return ERROR;
        }
    }
    public String DetalleHistorialPedidos() throws Exception{
        try{
            PedidoDAO pedidoDao = new PedidoDAO();
            listarDetallePedido = pedidoDao.ListarDetallePedidos(idPed);
        }catch(Exception e){
            System.out.println("error ---------------> DetalleHistorialPedidos "+e.getMessage());
            e.printStackTrace();
        }
        if(listarDetallePedido.isEmpty()==false){
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            return SUCCESS;
        }else{
            addActionError("error ---------------> DetalleHistorialPedidos");
            return ERROR;
        }
    }
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //METODOS PARA LOS REPORTES
    List<canVenMensualxAnio> canVenMensualxAnio=null;
    List<cantCompCli> cantCompCli=null;
    List<cantVentCliMesAnio> cantVentCliMesAnio=null;
    List<montCatAnual> montCatAnual=null;
    List<montFacxCliAnual> montFacxCliAnual=null;
    List<montoVendAnual> montoVendAnual=null;
    List<proMasVendCat> proMasVendCat=null;
    List<proTopAnual> proTopAnual=null;
    List<proTopFechas> proTopFechas=null;
    int anio;
    Date inicio;
    Date fin;
    public String canVenMensualxAnio() {
        try {
            boolean resultado=false;
            ReportesDAO VenMensualxAnio = new ReportesDAO();
            canVenMensualxAnio = VenMensualxAnio.canVenMensualxAnio(anio);
            System.out.println("exito -----> canVenMensualxAnio Action ");
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> canVenMensualxAnio Action"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String cantCompCli() {
        try {
            boolean resultado=false;
            ReportesDAO CompCli = new ReportesDAO();
            cantCompCli = CompCli.cantCompCli();
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> cantCompCli Action ");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> cantCompCli Action"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String cantVentCliMesAnio() {
        return null;
    }
    public String montCatAnual() {
        try {
            boolean resultado=false;
            ReportesDAO mCatAnual = new ReportesDAO();
            montCatAnual = mCatAnual.montCatAnual(anio);
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> canVenMensualxAnio montCatAnual ");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> canVenMensualxAnio montCatAnual"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String montFacxCliAnual() {
        try {
            ReportesDAO mFacxCliAnual = new ReportesDAO();
            montFacxCliAnual = mFacxCliAnual.montFacxCliAnual(anio);
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> montFacxCliAnual montCatAnual ");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> montFacxCliAnual montCatAnual"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String montoVendAnual() {
        try {
            ReportesDAO moVendAnual = new ReportesDAO();
            montoVendAnual = moVendAnual.montoVendAnual();
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> montoVendAnual Action ");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> montoVendAnual Action"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String proMasVendCat() {
        try {
            Categoria cat= new Categoria();
            cat.setIdCategoria(idCat);
            ReportesDAO reportesDao = new ReportesDAO();
            FechaIF fechas = new FechaIF();
            fechas.setCategoria(cat);
            fechas.setInicio(inicio);
            fechas.setFin(fin);
            proTopFechas = reportesDao.proMasVendCat(fechas);
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> proTopFechas Action");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> proTopFechas Action" + e.getMessage());
            
            return ERROR;
        }
    }
    public String proTopAnual() {
        try {
            ReportesDAO TopAnual = new ReportesDAO();
            proTopAnual = TopAnual.proTopAnual(anio);
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> proTopAnual Action ");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> proTopAnual Action"+e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
    public String proTopFechas() {
        try {
            ReportesDAO reportesDao = new ReportesDAO();
            FechaIF fechas = new FechaIF();
            fechas.setInicio(inicio);
            fechas.setFin(fin);
            proTopFechas = reportesDao.proTopFechas(fechas);
            Map ses = ActionContext.getContext().getSession();
            Usuario usu = new Usuario();
            usu = (Usuario)ses.get("sesionUsuario");
            int idtipo = usu.getTipo().getIdTipo();
            ListarPaginas(idtipo);
            System.out.println("exito -----> proTopFechas Action");
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("error -----> proTopFechas Action" + e.getMessage());
            
            return ERROR;
        }
    }
    
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //METODOS PARA ENVIAR CORREO DESPUES DE GUARDAR PEDIDO
    private String from ="loopezcesar@gmail.com";
    private String claveCorreo="8pajEd_Va";
    private String to;
    private String subject="Pedido-SystemStore";
    private String body;

   static Properties properties = new Properties();
   static
   {
      properties.put("mail.smtp.host", "smtp.gmail.com");
      properties.put("mail.smtp.socketFactory.port", "465");
      properties.put("mail.smtp.socketFactory.class",
                     "javax.net.ssl.SSLSocketFactory");
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.port", "465");
   }
    public String EnviarCorreo(){
       String ret = SUCCESS;
       String body1="";
       String body2="";
       String body3="";
       String body4="";
       String body5="";
       Map ses = ActionContext.getContext().getSession();
       Usuario u = (Usuario) ses.get("sesionUsuario");
       try{
          Session session = Session.getInstance(properties,  
            new javax.mail.Authenticator() {
            protected PasswordAuthentication 
            getPasswordAuthentication() {
            return new 
            PasswordAuthentication(from, claveCorreo);
            }});
          int iduser=u.getIdUsuario();
            UsuarioDAO user = new UsuarioDAO();
            Cliente c = new Cliente();
            c=user.listarDatosCliente(iduser);
            to=c.getTelefono();
            System.out.println("Correo"+to);
          body1=  "<table>"+
         "<thead>"+
            "<tr>"+
               "<th>Item</th>"+
               "<th>idProducto</th>"+
               "<th>Nombre Producto</th>"+
               "<th>Cantidad</th>"+
               "<th>Precio</th>"+
               "<th >Importe</th>"+
            "</tr>"+
         "</thead>"+
         "<tbody>";
      Vector<DetallePedido> listadetalle = null;
      listadetalle = (Vector<DetallePedido>)ses.get("sesDetalle");
      for(int i=0;i<listadetalle.size();i++){
                 body2+= "<tr>"+
                  "<td>"+listadetalle.get(i).getIdDetallePedido()+"</td>"+
                  "<td>"+listadetalle.get(i).getIdProducto() +"</td>"+
                  "<td>"+listadetalle.get(i).getNomProducto() +"</td>"+
                  "<td>"+listadetalle.get(i).getCantidad() +"</td>"+
                  "<td>"+listadetalle.get(i).getPrecio() +"</td>"+
                  "<td>"+listadetalle.get(i).getImporte()+"</td>"+
                   "</tr>";
}
                 if(listadetalle.size()!= 0){
                     body3="<td colspan='6' id='montototal'>Total a Pagar S/ :"+"  "+""+listadetalle.get(listadetalle.size()-1).getTotal()+"</td>";
                 }else{
                      body4="<td colspan='6' id='montototal'>Total a Pagar S/ :0.0</td>";
                 }
         body5="</tbody>"+
   "</table>";
            
         body=body1 + body2+body3+body4+body5;
         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(from));
         message.setRecipients(Message.RecipientType.TO, 
            InternetAddress.parse(to));
         message.setSubject(subject);
         message.setContent(body,"text/html");
         Transport.send(message);
         
         
       }
       catch(Exception e){
          ret = ERROR;
         System.out.println("ERROR:" + e.getMessage());
         e.printStackTrace();
       }
        return ret;
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //VARIABLES PARA EL GRID CON JSON
    private Integer rowa=0, page=0, total=0, records=0;
    private String sord, sidx, searchField, searchString, searchOper;
    private List<Usuario> gridModel;
    
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //VARIABLES GET AND SET
    
    public int getIdUsuario() {
        return idUsuario;
    }
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    public String getUsu() {
        return usu;
    }
    public void setUsu(String usu) {
        this.usu = usu;
    }
    public String getCla() {
        return cla;
    }
    public void setCla(String cla) {
        this.cla = cla;
    }
    public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip = tip;
    }
    public List<Pagina> getListarPaginas() {
        return listarPaginas;
    }
    public void setListarPaginas(List<Pagina> listarPaginas) {
        this.listarPaginas = listarPaginas;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public int getIdPro() {
        return idPro;
    }
    public void setIdPro(int idPro) {
        this.idPro = idPro;
    }
    public int getIdCat() {
        return idCat;
    }
    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }
    public int getSto() {
        return sto;
    }
    public void setSto(int sto) {
        this.sto = sto;
    }
    public int getCant() {
        return cant;
    }
    public void setCant(int cant) {
        this.cant = cant;
    }
    public double getPre() {
        return pre;
    }
    public void setPre(double pre) {
        this.pre = pre;
    }
    public double getAcumTotal() {
        return acumTotal;
    }
    public void setAcumTotal(double acumTotal) {
        this.acumTotal = acumTotal;
    }
    public String getNomPro() {
        return nomPro;
    }
    public void setNomPro(String nomPro) {
        this.nomPro = nomPro;
    }
    public String getEst() {
        return est;
    }
    public void setEst(String est) {
        this.est = est;
    }
    public String getCad() {
        return cad;
    }
    public void setCad(String cad) {
        this.cad = cad;
    }
    public String getNomCate() {
        return nomCate;
    }
    public void setNomCate(String nomCate) {
        this.nomCate = nomCate;
    }
    public Date getFec() {
        return fec;
    }
    public void setFec(Date fec) {
        this.fec = fec;
    }
    public Producto getProducto() {
        return producto;
    }
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    public List<String> getCategorias() {
        return categorias;
    }
    public void setCategorias(List<String> categorias) {
        this.categorias = categorias;
    }
    public List<Producto> getListarProductos() {
        return listarProductos;
    }
    public void setListarProductos(List<Producto> listarProductos) {
        this.listarProductos = listarProductos;
    }
    public List<Producto> getListarProductosRAN() {
        return listarProductosRAN;
    }
    public void setListarProductosRAN(List<Producto> listarProductosRAN) {
        this.listarProductosRAN = listarProductosRAN;
    }
    public List<Comentario> getListarComentProd() {
        return listarComentProd;
    }
    public void setListarComentProd(List<Comentario> listarComentProd) {
        this.listarComentProd = listarComentProd;
    }
    public List<canVenMensualxAnio> getCanVenMensualxAnio() {
        return canVenMensualxAnio;
    }
    public void setCanVenMensualxAnio(List<canVenMensualxAnio> canVenMensualxAnio) {
        this.canVenMensualxAnio = canVenMensualxAnio;
    }
    public List<cantCompCli> getCantCompCli() {
        return cantCompCli;
    }
    public void setCantCompCli(List<cantCompCli> cantCompCli) {
        this.cantCompCli = cantCompCli;
    }
    public List<cantVentCliMesAnio> getCantVentCliMesAnio() {
        return cantVentCliMesAnio;
    }
    public void setCantVentCliMesAnio(List<cantVentCliMesAnio> cantVentCliMesAnio) {
        this.cantVentCliMesAnio = cantVentCliMesAnio;
    }
    public List<montCatAnual> getMontCatAnual() {
        return montCatAnual;
    }
    public void setMontCatAnual(List<montCatAnual> montCatAnual) {
        this.montCatAnual = montCatAnual;
    }
    public List<montFacxCliAnual> getMontFacxCliAnual() {
        return montFacxCliAnual;
    }
    public void setMontFacxCliAnual(List<montFacxCliAnual> montFacxCliAnual) {
        this.montFacxCliAnual = montFacxCliAnual;
    }
    public List<montoVendAnual> getMontoVendAnual() {
        return montoVendAnual;
    }
    public void setMontoVendAnual(List<montoVendAnual> montoVendAnual) {
        this.montoVendAnual = montoVendAnual;
    }
    public List<proMasVendCat> getProMasVendCat() {
        return proMasVendCat;
    }
    public void setProMasVendCat(List<proMasVendCat> proMasVendCat) {
        this.proMasVendCat = proMasVendCat;
    }
    public List<proTopAnual> getProTopAnual() {
        return proTopAnual;
    }
    public void setProTopAnual(List<proTopAnual> proTopAnual) {
        this.proTopAnual = proTopAnual;
    }
    public List<proTopFechas> getProTopFechas() {
        return proTopFechas;
    }
    public void setProTopFechas(List<proTopFechas> proTopFechas) {
        this.proTopFechas = proTopFechas;
    }
    public int getAnio() {
        return anio;
    }
    public void setAnio(int anio) {
        this.anio = anio;
    }
    public Date getInicio() {
        return inicio;
    }
    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }
    public Date getFin() {
        return fin;
    }
    public void setFin(Date fin) {
        this.fin = fin;
    }
   public int getIdCliente() {
      return idCliente;
   }
   public void setIdCliente(int idCliente) {
      this.idCliente = idCliente;
   }
   public String getFrom() {
      return from;
   }
   public void setFrom(String from) {
      this.from = from;
   }

    public String getClaveCorreo() {
        return claveCorreo;
    }

    public void setClaveCorreo(String claveCorreo) {
        this.claveCorreo = claveCorreo;
    }
   
   public String getTo() {
      return to;
   }
   public void setTo(String to) {
      this.to = to;
   }
   public String getSubject() {
      return subject;
   }
   public void setSubject(String subject) {
      this.subject = subject;
   }
   public String getBody() {
      return body;
   }
   public void setBody(String body) {
      this.body = body;
   }
   public static Properties getProperties() {
      return properties;
   }
   public static void setProperties(Properties properties) {
      MetodosAction.properties = properties;
   }

    public String getNomCli() {
        return nomCli;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public String getApeCli() {
        return apeCli;
    }

    public void setApeCli(String apeCli) {
        this.apeCli = apeCli;
    }

    public String getCelCli() {
        return celCli;
    }

    public void setCelCli(String celCli) {
        this.celCli = celCli;
    }

    public String getEmailCli() {
        return emailCli;
    }

    public void setEmailCli(String emailCli) {
        this.emailCli = emailCli;
    }

    public String getDirCli() {
        return dirCli;
    }

    public void setDirCli(String dirCli) {
        this.dirCli = dirCli;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public Date getFechNacCli() {
        return fechNacCli;
    }

    public void setFechNacCli(Date fechNacCli) {
        this.fechNacCli = fechNacCli;
    }

    public List<Pedido> getListarPedidos() {
        return listarPedidos;
    }

    public void setListarPedidos(List<Pedido> listarPedidos) {
        this.listarPedidos = listarPedidos;
    }

    public int getIdPed() {
        return idPed;
    }

    public void setIdPed(int idPed) {
        this.idPed = idPed;
    }

    public List<DetallePedido> getListarDetallePedido() {
        return listarDetallePedido;
    }

    public void setListarDetallePedido(List<DetallePedido> listarDetallePedido) {
        this.listarDetallePedido = listarDetallePedido;
    }

    public Integer getRowa() {
        return rowa;
    }

    public void setRowa(Integer rowa) {
        this.rowa = rowa;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public List<Usuario> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Usuario> gridModel) {
        this.gridModel = gridModel;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public List<Producto> getListaProductoBaja() {
        return listaProductoBaja;
    }

    public void setListaProductoBaja(List<Producto> listaProductoBaja) {
        this.listaProductoBaja = listaProductoBaja;
    }
    
}
