<%-- 
    Document   : Catalogo
    Created on : 17-nov-2013, 15:06:04
    Author     : Cesar Lopez
--%>
<%@page import="java.util.List"%>
<%@page  import="com.store.bean_entity.*" %>
<%@page  import="com.store.action.MetodosAction" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
    <body>
       <header>
            <a href="index-2.html" class="logo">
                <img src="img/logo.png" alt="Logo"/>
            </a>
          
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
        </header>
       
        <div class="container wrapper" style="width: 80%">
           
        <div class="row">
            <div class="span12">
                    <h2>Catalogo de Productos</h2>
            </div>
        </div><br><br>
        <div class="row" style="margin-top: 50px; width: 100%;">
           <div class="span12" style="width: 100%">
               <span>Filtrar por categoria:</span>
                <ul class="top-nav"><!--LISTADO DE CATEGORIAS-->
                    
                </ul>
                
                <form action="busquedaCatalogoXML" class="form-search">
                        <input name="cad" required="true" class="input-medium search-query" />
                        <input type="submit" value="Buscar" />
        </form>
                <ul class="thumbnails portfolio ">
                   <s:iterator value="listarProductos" >
                      <form action="agregarCarritoXML">
                        <li class="span3 cat-2" style="margin-top: 40px; width: 300px; height:400px ">
                           <div class="thumbnail" style="height:300px;">
                             <a class="js-fancybox" rel="album" href="#">
                                 <img src="imgProducto/<s:property value="imagen"></s:property>" alt="">
                             </a>
                             <b><a>Nombre Producto: </a></b> <s:property value="nombre"></s:property><br>
                             <b><a>Categoria: </a></b><s:property value="Categoria.nomCategoria"></s:property><br>
                             <b><a>Precio: </a></b> <s:property value="precio"></s:property><br>
                             <s:textfield placeholder="cantidad" name="cant" ></s:textfield><br>
                             <s:url id="url" action="detalleProdXML">
                                    <s:param name="idPro">
                                        <s:property value="idProducto"></s:property>
                                    </s:param>
                                    <s:param name="nomCate">
                                        <s:property value="Categoria.nomCategoria"></s:property>
                                    </s:param>
                             </s:url>
                             <s:a cssClass="btn btn-success bottom-margin" href="%{url}" >Ver Detalles</s:a>
                             <s:hidden name="idPro" value="%{idProducto}"></s:hidden>
                             <s:hidden name="pre" value="%{precio}"></s:hidden>
                             <s:hidden name="nomPro" value="%{nombre}"></s:hidden>
                             
                             <sj:submit  name="btnagregar" value="agregar" cssClass="btn btn-info" ></sj:submit><br><br>
                        </div>
                        </li>
                      </form>
                    </s:iterator>
                   
                </ul>
                </div>
        </div>
        </div>
    </body>
</html>