package com.store.bean_report;

public class montCatAnual {
    String nomCategoria;
    double totfacturado;

    public String getNomCategoria() {
        return nomCategoria;
    }

    public void setNomCategoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }

    public double getTotfacturado() {
        return totfacturado;
    }

    public void setTotfacturado(double totfacturado) {
        this.totfacturado = totfacturado;
    }
    
}
