<%-- 
    Document   : CarritoCompras
    Created on : 28-nov-2013, 17:17:13
    Author     : Cesar Lopez
--%>

<%@page import="java.util.Vector"%>
<%@page import="com.store.bean_entity.DetallePedido"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
   <body>
   <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div><center>
       <h1>Carrito Compras</h1>
      <form action="GuardarPedidoXML">
          <table border="0" id="tablaCarrito" class="table table-striped table-bordered no-margin" style="width: 80%; " >
         <thead>
            <tr>
               <th>Item</th>
               <th>idProducto</th>
               <th>Nombre Producto</th>
               <th>Cantidad</th>
               <th>Precio</th>
               <th >Importe</th>
               <th>Opcion</th>
            </tr>
         </thead>
         <tbody>
              <%
               Vector <DetallePedido> listadetalle = (Vector<DetallePedido>)session.getAttribute("sesDetalle");
                 for(int i=0;i<listadetalle.size();i++){
                  out.print("<tr>");
                  out.print("<td>"+listadetalle.get(i).getIdDetallePedido()+"</td>");
                  out.print("<td>"+listadetalle.get(i).getIdProducto() +"</td>");
                  out.print("<td>"+listadetalle.get(i).getNomProducto() +"</td>");
                  out.print("<td>"+listadetalle.get(i).getCantidad() +"</td>");
                  out.print("<td>"+listadetalle.get(i).getPrecio() +"</td>");
                  out.print("<td>"+listadetalle.get(i).getImporte()+"</td>");
                  out.print("<td><a href='detalleProdXML?idPro="+listadetalle.get(i).getIdProducto()+"'>Ver</a></td>");
                  out.print("<td><a href='eliminarProductoCarritoXML?idPro="+listadetalle.get(i).getIdDetallePedido()+"'>Quitar</a></td>");
                  out.print("</tr>");
              }
                 if(listadetalle.size()!= 0){
                     out.print("<td colspan='6' id='montototal'>Total a Pagar S/ :"+"  "+""+listadetalle.get(listadetalle.size()-1).getTotal()+"</td>");
                 }else{
                      out.print("<td colspan='6' id='montototal'>Total a Pagar S/ :0.0</td>");
                 }
              %>
         </tbody>
         <tfoot>
             <tr>
                 <td>
                     <input type="submit" value="Guardar Pedido"/><br/>
                  </td>
             </tr>
         </tfoot>
      </table>
      </form></center>
   </body>
</html>
