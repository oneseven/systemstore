<%-- 
    Document   : HistorialPedido
    Created on : 30-nov-2013, 12:06:30
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
   <body>
        <s:actionerror theme="bootstrap"/>
        <s:actionmessage theme="bootstrap"/>
        <s:fielderror theme="bootstrap"/>
        <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
   <center><h1>Historial de Pedidos</h1></center>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div><%-- CIERRA DIV DE MENU --%>
        <center>
        <div class="widget-body">
        <table  class="table table-striped table-bordered no-margin" style="width: 60%;">
                <th colspan="10">Listar Historial de Pedidos</th>
                <tr class="success">
                    <td align="center">Codigo Pedido</td>
                    <td align="center">Fecha Compra</td>
                    <td align="center">Fecha estimada de envio</td>
                    <td align="center">Monto total</td>
                </tr>
            <s:iterator value="listarPedidos">
                <tr >
                    <td align="center"><s:property value="idPedido"></s:property></td>
                    <td align="center"><s:property value="fechaCompra"></s:property></td>
                    <td align="center"><s:property value="fechaEnvio"></s:property></td>
                    <td  align="center"><s:property value="montoTotal"></s:property></td>
                    <td>
                    <s:url id="url" action="DetallePedidoXML">
                        <s:param name="idPed">
                            <s:property value="idPedido"></s:property>
                        </s:param>
                    </s:url>
                    <s:a href="%{url}" cssClass="fs1" aria-hidden="true" data-icon="" >Ver Detalle</s:a>
                    </td>
                    <td>
                    <s:url id="url2" action="eliminaProductoXML">
                        <s:param name="idPed">
                            <s:property value="idPedido"></s:property>
                        </s:param>
                    </s:url>
                    <s:a href="%{url2}" cssClass="fs1" aria-hidden="true" data-icon="">Realizado</s:a>
                    </td>
                </tr>
            </s:iterator>
            </table>
        </div>
   </center>
    </body>
</html>
