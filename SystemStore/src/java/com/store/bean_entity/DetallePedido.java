package com.store.bean_entity;

public class DetallePedido {
    int idDetallePedido;
    int idPedido;
    int idProducto;
    int cantidad;
    double precio;
    double importe;
    
    String nomProducto;
    Double Total;

    public int getIdDetallePedido() {
        return idDetallePedido;
    }

    public void setIdDetallePedido(int idDetallePedido) {
        this.idDetallePedido = idDetallePedido;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

   public String getNomProducto() {
      return nomProducto;
   }

   public void setNomProducto(String nomProducto) {
      this.nomProducto = nomProducto;
   }

   public Double getTotal() {
      return Total;
   }

   public void setTotal(Double Total) {
      this.Total = Total;
   }
    
    
}
