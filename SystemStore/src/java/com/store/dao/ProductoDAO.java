package com.store.dao;


import com.store.bean_entity.Categoria;
import com.store.bean_entity.Producto;
import com.store.service.ProductoInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProductoDAO implements ProductoInterfaz{
    
    @Override
    public List ListarProductos() throws Exception {
        List listadoProductos = new ArrayList();
        CallableStatement cstm = null;
        ResultSet rst = null;
        Connection cnn = null;
        try{
            cnn = Conexion.getConnection();
            String comando = "CALL sp_Listar_producto()";
            cstm = cnn.prepareCall(comando);
            rst = cstm.executeQuery();
            while(rst.next()){
                Producto producto = new Producto();
                producto.setIdProducto(rst.getInt(1));
                Categoria categoria = new Categoria();
                categoria.setNomCategoria(rst.getString(2));
                producto.setCategoria(categoria);
                producto.setNombre(rst.getString(3));
                producto.setStock(rst.getInt(4));
                producto.setPrecio(rst.getDouble(5));
                producto.setFechaCaduca(rst.getDate(6));
                producto.setEstado(rst.getString(7));
                producto.setImagen(rst.getString(8));
                listadoProductos.add(producto);
            }
            System.out.println(" exito dao -------> ListarProductos ");
        }catch(Exception e){
            System.out.println(" error dao -------> ListarProductos "+e.getMessage());
            e.printStackTrace();
        }
        return listadoProductos;
    }

    @Override
    public boolean ingresoProducto(Producto producto) throws Exception {
        System.out.println("entro al dao de ingreso");
        Connection con=null;
        CallableStatement cstm=null;
        try {
            System.out.println("entro al try de ingreso DAO");
            con=Conexion.getConnection();
            String sql="CALL sp_add_Producto(?,?,?,?,?,?,?)";
            cstm=con.prepareCall(sql);
            cstm.setInt(1,producto.getCategoria().getIdCategoria());
            cstm.setString(2,producto.getNombre());
            cstm.setInt(3,producto.getStock());
            cstm.setDouble(4,producto.getPrecio());
            cstm.setDate(5, producto.getFechaCaduca());
            cstm.setString(6, producto.getEstado());
            cstm.setString(7,producto.getImagen());
            cstm.execute();
            System.out.println(" exito dao -------> ingresoProductoDAO ");
            return true;
        } catch (Exception e) {
            System.out.println(" error dao -------> ingresoProductoDAO "+e.getMessage());
            e.printStackTrace();
            return false;
        }finally{
            cstm.close();
            con.close();
        }
    }

    @Override
    public boolean EliminarProducto(int id) throws Exception {
        Connection cnn = null;
        CallableStatement cstm = null;
        try{
            cnn = Conexion.getConnection();
            String Comando = "call sp_Borrar_producto(?);";
            cstm = cnn.prepareCall(Comando);
            cstm.setInt(1, id);
            cstm.execute();
            System.out.println(" exito dao -------> EliminarProducto ");
            return true;
        }catch(Exception e){
            System.out.println(" error dao -------> EliminarProducto ");
            return false;
        }
    }

    @Override
    public boolean ActuaizarProductoDao(Producto producto) throws Exception {
        Connection cn = null;
        CallableStatement cstm = null;
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_Actualizar_producto(?,?,?,?,?,?,?);";
            cstm = cn.prepareCall(Comando);
            cstm.setInt(1, producto.getIdProducto());
            cstm.setInt(2, producto.getCategoria().getIdCategoria());
            cstm.setString(3,producto.getNombre());
            cstm.setInt(4, producto.getStock());
            cstm.setString(7, producto.getEstado());
            cstm.setDouble(5,producto.getPrecio());
            cstm.setDate(6, producto.getFechaCaduca());
            cstm.execute();
            System.out.println(" exito dao -------> ActuaizarProductoDao ");
            return true;
        }catch(Exception e){
            System.out.println(" error dao -------> ActuaizarProductoDao  ");
            return false;
        }
    }

    @Override
    public Producto BuscarProductoID(int id) throws Exception {
        Connection cn = null;
        CallableStatement cstm = null;
        ResultSet rst = null;
        Producto producto = new Producto();
        Categoria categoria = new Categoria();
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_producto_Por_Codigo(?);";
            cstm = cn.prepareCall(Comando);
            cstm.setInt(1, id);
            rst = cstm.executeQuery();
            while(rst.next()){
                producto.setIdProducto(rst.getInt(1));
                categoria.setIdCategoria(rst.getInt(2));
                producto.setCategoria(categoria);
                producto.setNombre(rst.getString(3));
                producto.setStock(rst.getInt(4));
                producto.setPrecio(rst.getInt(5));
                producto.setFechaCaduca(rst.getDate(6));
                producto.setEstado(rst.getString(7));
                producto.setImagen(rst.getString(8));
            }
        }catch(Exception e){
            System.out.println(" error dao -------> BuscarProductoID  ");
        }
        return producto;
    }

    @Override
    public List BuscarProductoNombre(String cad) throws Exception {
        List listadoBuscaProducto = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_producto_Por_Nombre(?);";
            cstm = con.prepareCall(sql);
            cstm.setString(1, cad);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdProducto(rs.getInt(1));
                Categoria categoria = new Categoria();
                categoria.setNomCategoria(rs.getString(2));
                producto.setCategoria(categoria);
                producto.setNombre(rs.getString(3));
                producto.setStock(rs.getInt(4));
                producto.setEstado(rs.getString(5));
                producto.setPrecio(rs.getDouble(6));
                producto.setFechaCaduca(rs.getDate(7));
                producto.setImagen(rs.getString(8));
                listadoBuscaProducto.add(producto);
                System.out.println(" exito dao -------> BuscarProductoNombre ");
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> BuscarProductoNombre  ");
            e.printStackTrace();
        }
        return listadoBuscaProducto;
    }

    @Override
    public List ListadoProductoxCategoria(int id) throws Exception {
        List ListadoProductoxCategoria = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_Listar_producto_Por_Categoria(?)";
            cstm = con.prepareCall(sql);
            cstm.setInt(1, id);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdProducto(rs.getInt(1));
                Categoria categoria = new Categoria();
                categoria.setNomCategoria(rs.getString(2));
                producto.setCategoria(categoria);
                producto.setNombre(rs.getString(3));
                producto.setStock(rs.getInt(4));
                producto.setEstado(rs.getString(5));
                producto.setPrecio(rs.getDouble(6));
                producto.setFechaCaduca(rs.getDate(7));
                ListadoProductoxCategoria.add(producto);
                System.out.println(" exito dao -------> ListadoProductoxCategoria ");
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> ListadoProductoxCategoria  ");
            e.printStackTrace();
        }
        return ListadoProductoxCategoria;
    }

    @Override
    public List ListadoProductoRAN(String nomCate) throws Exception {
        List listadoBuscaProductoRAN = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_search_ProdxNomCate(?);";
            cstm = con.prepareCall(sql);
            cstm.setString(1, nomCate);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdProducto(rs.getInt(1));
                Categoria categoria = new Categoria();
                categoria.setNomCategoria(rs.getString(2));
                producto.setCategoria(categoria);
                producto.setNombre(rs.getString(3));
                producto.setStock(rs.getInt(4));
                producto.setEstado(rs.getString(5));
                producto.setPrecio(rs.getDouble(6));
                producto.setFechaCaduca(rs.getDate(7));
                producto.setImagen(rs.getString(8));
                listadoBuscaProductoRAN.add(producto);
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> BuscarProductoNombre  " + e.getMessage());
            e.printStackTrace();
        }
        return listadoBuscaProductoRAN;
    }

    @Override
    public List ListadoProductoBaja() throws Exception {
        List listadoProductosBaja = new ArrayList();
        CallableStatement cstm = null;
        ResultSet rst = null;
        Connection cnn = null;
        try{
            cnn = Conexion.getConnection();
            String comando = "CALL sp_lista_Prod_Baja()";
            cstm = cnn.prepareCall(comando);
            rst = cstm.executeQuery();
            while(rst.next()){
                Producto producto = new Producto();
                producto.setIdProducto(rst.getInt(1));
                Categoria categoria = new Categoria();
                categoria.setNomCategoria(rst.getString(2));
                producto.setCategoria(categoria);
                producto.setNombre(rst.getString(3));
                producto.setStock(rst.getInt(4));
                producto.setPrecio(rst.getDouble(5));
                producto.setFechaCaduca(rst.getDate(6));
                producto.setEstado(rst.getString(7));
                producto.setImagen(rst.getString(8));
                listadoProductosBaja.add(producto);
            }
            System.out.println(" exito dao -------> ListarProductos ");
        }catch(Exception e){
            System.out.println(" error dao -------> ListarProductos "+e.getMessage());
            e.printStackTrace();
        }
        return listadoProductosBaja;
    }
    
}
