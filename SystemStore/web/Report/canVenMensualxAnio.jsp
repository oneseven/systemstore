<%-- 
    Document   : canVenMensualxAnio
    Created on : BARRAS
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="ch" uri="/struts-jquery-chart-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <link href="../Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="../Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <title>Reporte de Ventas</title>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="../Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
    <center> <br> <br>
        <s:form action="canVenMensualxAnioXML">
            ingrese el año <s:textfield name="anio" label="Año" ></s:textfield>
            <s:submit value="enviar"></s:submit>
        </s:form>
    </center>
        <div id="cont">
        <h1>Cantidad de venta mensual por año</h1>
        <ch:chart id="chartPoints" draggable="true" autoResize="true">
            <s:iterator value="canVenMensualxAnio">
                <ch:chartData id="chardata1" label="Enero" bars="{show : true, barWidth: 0.5}" data="[[1,%{enero}]]" />
                <ch:chartData id="chardata2" label="Febrero" bars="{show : true, barWidth: 0.5}" data="[[2,%{febrero}]]"/>
                <ch:chartData id="chardata3" label="Marzo" bars="{show : true, barWidth: 0.5}" data="[[3,%{marzo}]]"/> 
                <ch:chartData id="chardata4" label="Abril" bars="{show : true, barWidth: 0.5}" data="[[4,%{abril}]]"/> 
                <ch:chartData id="chardata5" label="Mayo" bars="{show : true, barWidth: 0.5}" data="[[5,%{mayo}]]"/> 
                <ch:chartData id="chardata6" label="Junio" bars="{show : true, barWidth: 0.5}" data="[[6,%{junio}]]"/> 
                <ch:chartData id="chardata7" label="Julio" bars="{show : true, barWidth: 0.5}" data="[[7,%{julio}]]"/> 
                <ch:chartData id="chardata8" label="Agosto" bars="{show : true, barWidth: 0.5}" data="[[8,%{agosto}]]"/>
                <ch:chartData id="chardata9" label="Setiembre" bars="{show : true, barWidth: 0.5}" data="[[9,%{setiembre}]]"/> 
                <ch:chartData id="chardata10" label="Octubre" bars="{show : true, barWidth: 0.5}" data="[[10,%{octubre}]]"/> 
                <ch:chartData id="chardata11" label="Noviembre" bars="{show : true, barWidth: 0.5}" data="[[11,%{noviembre}]]"/> 
                <ch:chartData id="chardata12" label="Diciembre" bars="{show : true, barWidth: 0.5}" data="[[12,%{diciembre}]]"/>
            </s:iterator>
        </ch:chart>
        </div>
        <a href="Reportes.jsp"><div id="btn">Regresar</div></a>
    </body>
</html>
