package com.store.dao;


import com.store.bean_entity.Categoria;
import com.store.service.CategoriaInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAO implements CategoriaInterfaz {
    
    @Override
    public List ListarCategorias() throws Exception {
        List listadoCategorias = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_Listar_categoria();";
            cstm = con.prepareCall(sql);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Categoria categoria = new Categoria();
                categoria.setIdCategoria(rs.getInt(1));
                categoria.setNomCategoria(rs.getString(2));
                listadoCategorias.add(categoria);
            }
            System.out.println("exito -------------> ListarCategorias ");
        } catch (Exception e) {
            System.out.println("error -------------> ListarCategorias" + e.getMessage());
            e.printStackTrace();
        } finally {
            rs.close();
            cstm.close();
            con.close();
        }
        return listadoCategorias;
    }

    @Override
    public boolean ingresoCategoriaDAO(Categoria categoria) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
