package com.store.service;


import com.store.bean_report.FechaIF;

import java.util.List;

public interface ReportesInterfaz {
    
    public List cantCompCli()throws Exception;
    public List canVenMensualxAnio(int n)throws Exception;
    public List montCatAnual(int n)throws Exception;
    public List montFacxCliAnual(int n)throws Exception;
    public List montoVendAnual()throws Exception;
    public List proTopAnual(int n)throws Exception;
    public List proTopFechas(FechaIF fechaif)throws Exception;
    public List proMasVendCat(FechaIF fechaif)throws Exception;
    public List cantVentCliMesAnio(int a, int b)throws Exception;
    
    
}
