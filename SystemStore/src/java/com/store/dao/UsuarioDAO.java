package com.store.dao;

import com.store.bean_entity.Cliente;
import com.store.bean_menu.Pagina;
import com.store.bean_menu.Tipo;
import com.store.bean_menu.Usuario;
import com.store.service.UsuarioInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class UsuarioDAO implements UsuarioInterfaz{

    @Override
    public Usuario ValidarUsuario(String User, String Clave) throws Exception {
        Connection Cnn = null;
        CallableStatement Cstm = null;
        ResultSet Rst = null;
        Usuario usuario = new Usuario();
        Tipo t = new Tipo();
        try{
            Cnn = Conexion.getConnection();
            String Comando = "call pa_ValidarUsuario(?,?)";
            Cstm = Cnn.prepareCall(Comando);
            Cstm.setString(1, User);
            Cstm.setString(2, Clave);
            Rst = Cstm.executeQuery();
            while(Rst.next()){
                usuario.setUsuario(Rst.getString(1));
                t.setIdTipo(Rst.getInt(2));
                usuario.setIdUsuario(Rst.getInt(3));
                usuario.setTipo(t);
            }
            System.out.println("exito login en ----> ValidarUsuario ");
        }catch(Exception e){
            System.out.println("error login en ----> ValidarUsuario");
        }
        return usuario;
    }

    @Override
    public List ListarPagina(int Tipo) throws Exception {
        List listadoPaginas = new ArrayList();
        Connection Cnn = null;
        CallableStatement Cstm = null;
        ResultSet Rst = null;
        try{
            Cnn = Conexion.getConnection();
            String Comando = "call pa_ListaPagina(?)";
            Cstm = Cnn.prepareCall(Comando);
            Cstm.setInt(1, Tipo);
            Rst = Cstm.executeQuery();
            while(Rst.next()){
                Pagina p = new Pagina();
                p.setIdPagina(Rst.getInt(1));
                p.setTitulo(Rst.getString(2));
                p.setUrl(Rst.getString(3));
                listadoPaginas.add(p);
            }
            System.out.println("Exito Listar Paginas");
        }catch(Exception e){
            System.out.println("Error Listar Paginas");
        }
        return listadoPaginas;
    }

    @Override
    public Cliente listarDatosCliente(int idusuario) throws Exception {
        Connection cn = null;
        CallableStatement cstm = null;
        ResultSet rst = null;
        Cliente cliente = new Cliente();
        Usuario usu = new Usuario();
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_listar_clientePorUsuario(?);";
            cstm = cn.prepareCall(Comando);
            cstm.setInt(1, idusuario);
            rst = cstm.executeQuery();
            while(rst.next()){
                cliente.setIdCliente(rst.getInt(1));
                usu.setIdUsuario(rst.getInt(2));
                cliente.setUsuario(usu);
                cliente.setNombre(rst.getString(3));
                cliente.setApellidos(rst.getString(4));
                cliente.setDni(rst.getInt(5));
                cliente.setCelular(rst.getString(6));
                cliente.setTelefono(rst.getString(7));
                cliente.setFecNacimniento(rst.getDate(8));
                cliente.setDireccion(rst.getString(9));
            }
        }catch(Exception e){
            System.out.println(" error dao -------> BuscarProductoID  ");
        }
        return cliente;
    }

   @Override
   public boolean nuevoUsuario(String User, String Clave) throws Exception {
        Connection con=null;
        CallableStatement cstm=null;
        try {
            System.out.println("entro al try de ingreso DAO");
            con=Conexion.getConnection();
            String sql="CALL sp_nuevoUsuario(?,?,?)";
            cstm=con.prepareCall(sql);
            int idtipo = 3;
            cstm.setInt(1,idtipo);
            cstm.setString(2,User);
            cstm.setString(3,Clave);
            cstm.execute();
            System.out.println(" exito dao -------> nuevoUsuario ");
            return true;
        } catch (Exception e) {
            System.out.println(" error dao -------> nuevoUsuario "+e.getMessage());
            e.printStackTrace();
            return false;
        }finally{
            cstm.close();
            con.close();
        }
   }

    @Override
    public boolean insertCliente(Cliente cliente) throws Exception {
        Connection con=null;
        CallableStatement cstm=null;
        try {
            System.out.println("entro al try de ingreso DAO");
            con=Conexion.getConnection();
            String sql="CALL sp_insertCliente(?,?,?,?,?,?,?,?)";
            cstm=con.prepareCall(sql);
            cstm.setInt(1,cliente.getUsuario().getIdUsuario());
            cstm.setString(2,cliente.getNombre());
            cstm.setString(3,cliente.getApellidos());
            cstm.setDouble(4,cliente.getDni());
            cstm.setString(5, cliente.getCelular());
            cstm.setString(6, cliente.getTelefono());
            cstm.setDate(7, cliente.getFecNacimniento());
            cstm.setString(8, cliente.getDireccion());
            cstm.execute();
            System.out.println(" exito dao -------> insertCliente ");
            return true;
        }catch (Exception e) {
            System.out.println(" error dao -------> insertCliente "+e.getMessage());
            e.printStackTrace();
            return false;
        }finally{
            cstm.close();
            con.close();
        }
    }

    @Override
    public boolean updateCliente(Cliente cliente) throws Exception {
        Connection cn = null;
        CallableStatement cstm = null;
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_updateCliente(?,?,?,?,?,?,?,?);";
            cstm = cn.prepareCall(Comando);
            cstm.setInt(1,cliente.getIdCliente());
            cstm.setString(2,cliente.getNombre());
            cstm.setString(3,cliente.getApellidos());
            cstm.setDouble(4,cliente.getDni());
            cstm.setString(5, cliente.getCelular());
            cstm.setString(6, cliente.getTelefono());
            cstm.setDate(7, cliente.getFecNacimniento());
            cstm.setString(8, cliente.getDireccion());
            cstm.execute();
            System.out.println(" exito dao -------> updateCliente ");
            return true;
        }catch(Exception e){
            System.out.println(" error dao -------> updateCliente  "+e.getMessage());
            return false;
        }
    }

    @Override
    public List ListarUsuarios() throws Exception {
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        List listadoUsuarios = new ArrayList();
        
        try{
            con = Conexion.getConnection();
            String sql = "CALL sp_listarUsuarios()";
            cstm = con.prepareCall(sql);
            rs = cstm.executeQuery();
            while(rs.next()){
                Usuario us = new Usuario();
                Tipo ti = new Tipo();
                us.setIdUsuario(rs.getInt(1));
                ti.setNombreTipo(rs.getString(2));
                us.setTipo(ti);
                us.setUsuario(rs.getString(3));
                us.setNomUsuario(rs.getString(4));
                us.setApeUsuario(rs.getString(5));
                listadoUsuarios.add(us);
            }
            System.out.println("Éxito listado dao ----> ListarUsuarios ");
            
        }catch(Exception e){
            System.out.println("Error listado UsuarioDAO"+e.getMessage());
            e.printStackTrace();
        }
        return listadoUsuarios;
    }
    
}
