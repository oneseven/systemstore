<%-- 
    Document   : nuevoUsuario
    Created on : 29-nov-2013, 21:21:20
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Blue Moon - Responsive Admin Dashboard</title>
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
        <script type="text/javascript" src="../../html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
    </head>
   <body>
        <header>
            <a class="logo">
               <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
      <div class="container-fluid">
      <div class="dashboard-container">
        <div class="top-nav">
          <ul>
            <li>
                <a href="login.jsp">
                <div class="fs1" aria-hidden="true" data-icon="&#xe0a0;"></div>
                Principal
              </a>
            </li>
            <li>
              <a href="forms.html" >
                <div class="fs1" aria-hidden="true" data-icon="&#xe0b8;"></div>
                Catalogo
              </a>
            </li>
            <li>
               <a href="CRUD/nuevoUsuario.jsp">
                <div class="fs1" aria-hidden="true" data-icon="&#xe096;"></div>
                Registarne
              </a>
            </li>
            <li>
              <a href="ui-elements.html">
                <div class="fs1" aria-hidden="true" data-icon="&#xe0d2;"></div>
                Nosotros
              </a>
            </li>
            <li>
              <a href="ui-elements.html">
                <div class="fs1" aria-hidden="true" data-icon="&#xe0d2;"></div>
                Ayuda
              </a>
            </li>
          </ul>
          <div class="clearfix">
          </div>
        </div>
        
        <div class="dashboard-wrapper">
          <div class="left-sidebar">
            
            <div class="row-fluid">
              
              <div class="span12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      Login
                    </div>
                    <span class="tools">
                      <a class="fs1" aria-hidden="true" data-icon="&#xe090;"></a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div class="span3">&nbsp;</div>
                    <div class="span6">
                      <div class="sign-in-container">
                      <s:form action="addUsuarioXML" cssStyle="login-wrapper">
                          <div class="header">
                            <div class="row-fluid">
                              <div class="span12">
                                <h3>Registrarme<img src="img/logo1.png" alt="Logo" class="pull-right"></h3>
                                <p>Ingrese un usuario y clave</p>
                              </div>
                            </div>
                          </div>
                            <s:textfield label="usuario" name="usu" placeholder="Email" required="required" class="input span12 email" ></s:textfield>
                            <s:password label="contraseña" name="cla" cssClass="input span12 password" placeholder="Password" required="required"></s:password>
                        </div>
                          <div class="actions">
                          <s:submit cssClass="btn btn-primary bottom-margin" value="Entrar"></s:submit>
                          <div class="clearfix"></div>
                          </div>
                        </s:form>
                      </div>
                    </div>
                    <div class="span3">&nbsp;</div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    </body>
</html>
