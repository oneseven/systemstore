package com.store.dao;


import com.store.bean_entity.Comentario;
import com.store.bean_menu.Usuario;
import com.store.service.MensajeInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MensajeDAO implements MensajeInterfaz{

    @Override
    public List ListarComentarioProd(int idpro) throws Exception {
        List listadoBuscaProducto = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_listar_comentario_producto(?);";
            cstm = con.prepareCall(sql);
            cstm.setInt(1, idpro);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Comentario comentario = new Comentario();
                comentario.setContenido(rs.getString(2));
                Usuario usuario = new Usuario();
                usuario.setUsuario(rs.getString(1));
                comentario.setUsuario(usuario);
                comentario.setFecha(rs.getDate(3));
                listadoBuscaProducto.add(comentario);
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> BuscarProductoNombre  " + e.getMessage());
            e.printStackTrace();
        }
        return listadoBuscaProducto;
    }

    @Override
    public boolean ingresoComentario(Comentario comen) throws Exception {
        Connection con=null;
        CallableStatement cstm=null;
        try{
            con=Conexion.getConnection();
            String sql="call sp_addComentario(?,?,?);";
            cstm=con.prepareCall(sql);
            cstm.setInt(1,comen.getUsuario().getIdUsuario());
            cstm.setInt(2,comen.getProducto().getIdProducto());
            cstm.setString(3,comen.getContenido());
            cstm.execute();
            System.out.println("Exito ingreso ComentarioDao");
            return true;
        }
        catch(Exception e){
            System.out.println("Exito ingreso ComentarioDao"+e.getMessage());
            e.printStackTrace();
            return false;
        }finally{
            cstm.close();
            con.close();
        }
    }
    
}
