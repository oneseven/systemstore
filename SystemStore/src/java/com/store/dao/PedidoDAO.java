package com.store.dao;


import com.store.bean_entity.DetallePedido;
import com.store.bean_entity.Pedido;
import com.store.service.PedidoInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PedidoDAO implements PedidoInterfaz{

    @Override
    public boolean IngresoPedido(Pedido p, List<DetallePedido> ListaPedido) throws Exception {
        Connection cn = null;
        CallableStatement cstm;
        int idPedido = 0;
        System.out.println("entro al DAO en ---> IngresoPedido");
        try {
            //ingreso pedido cabe
            cn = Conexion.getConnection();
            String sql  = "call sp_ingresoVentaCabe(?,?,?)";
            cstm  = cn.prepareCall(sql);
            cstm.setInt(1,p.getIdCliente());
            cstm.setDouble(2,p.getMontoTotal());
            cstm.setString(3,p.getEstado());
            cstm.execute();
            System.out.println("exito al ingresar la CABECERA");
            //recupero el id del pedido ingresado
            String sql1 = "call sp_UltimoIdVenta()";
            cstm = cn.prepareCall(sql1);
            ResultSet rs = cstm.executeQuery();
            while (rs.next()) {                
               idPedido = rs.getInt(1); 
               System.out.println("Ultimo idVenta ingresado --->" +idPedido);
            }
            //ingreso pedido-Deta
            for (int i = 0; i < ListaPedido.size(); i++) {
               String sql2= "call sp_IngresoDetalleVenta(?,?,?,?,?)";
               cstm = cn.prepareCall(sql2); 
               cstm.setInt(1,idPedido);
               cstm.setInt(2,ListaPedido.get(i).getIdProducto());
               cstm.setInt(3,ListaPedido.get(i).getCantidad());
               cstm.setDouble(4,ListaPedido.get(i).getPrecio());
               cstm.setDouble(5,ListaPedido.get(i).getImporte());
               cstm.execute();
               System.out.println("exito al ingresar el DETALLE VENTA");
            }
            System.out.println("Ingreso de Peido Cabe-Deta con Exito DAO");
            
            return true;
        } catch (Exception e) {
            System.out.println("Error al Momento de Ingresar Pedido Cabe-Deta DAO");
            System.out.println(e.getStackTrace());
            return false;
        }
    }
    
    public int devuelveidPedido(){
            Connection cn = null;
            int idPedido=0;
          try {
                cn = Conexion.getConnection();
                String sql1 = "call sp_UltimoIdVenta()";
                CallableStatement  cstm = cn.prepareCall(sql1);
                ResultSet rs = cstm.executeQuery();
             while (rs.next()) {                
               idPedido = rs.getInt(1); 
            }
             return idPedido;
        } catch (Exception e) {
              System.out.println("No se Recupero el ID VENTA para E-Mail");
              return idPedido;
        }
    }

    @Override
    public List ListarPedidos(int idcli) throws Exception {
        List historialPedidos = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_listaHisorialPedido(?)";
            cstm = con.prepareCall(sql);
            cstm.setInt(1, idcli);
            rs = cstm.executeQuery();
            while (rs.next()) {
                Pedido producto = new Pedido();
                producto.setIdPedido(rs.getInt(1));
                producto.setFechaCompra(rs.getDate(2));
                producto.setFechaEnvio(rs.getDate(3));
                producto.setMontoTotal(rs.getDouble(4));
                historialPedidos.add(producto);
                System.out.println(" exito dao -------> ListarPedidos ");
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> ListarPedidos  "+e.getMessage());
            e.printStackTrace();
        }
        return historialPedidos;
    }

    @Override
    public List ListarDetallePedidos(int idven) throws Exception {
        List historialDetallePedido = new ArrayList();
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;
        try {
            con = Conexion.getConnection();
            String sql = "call sp_HistorialDetallePedido(?)";
            cstm = con.prepareCall(sql);
            cstm.setInt(1, idven);
            rs = cstm.executeQuery();
            while (rs.next()) {
                DetallePedido detalle = new DetallePedido();
                detalle.setIdProducto(rs.getInt(1));
                detalle.setNomProducto(rs.getString(2));
                detalle.setCantidad(rs.getInt(3));
                detalle.setPrecio(rs.getDouble(4));
                detalle.setImporte(rs.getDouble(5));
                historialDetallePedido.add(detalle);
                System.out.println(" exito dao -------> ListarDetallePedidos ");
            }
        } catch (Exception e) {
            System.out.println(" error dao -------> ListarDetallePedidos  "+e.getMessage());
            e.printStackTrace();
        }
        return historialDetallePedido;
    }
}
