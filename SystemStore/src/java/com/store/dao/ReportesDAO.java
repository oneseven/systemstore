package com.store.dao;

import com.store.bean_report.*;
import com.store.service.ReportesInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ReportesDAO implements ReportesInterfaz{

    @Override
    public List cantCompCli() throws Exception {
        List cantCompCli=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_rep_cantCompCli()";
            CallableStatement cs=con.prepareCall(sql);
            ResultSet rs=cs.executeQuery();
            int cont=0;
            while (rs.next()) {
                cont +=1;
                cantCompCli producto = new cantCompCli();
                producto.setNombre(rs.getString(1));
                producto.setCantidad(rs.getInt(2));
                producto.setPosicion(cont);
                cantCompCli.add(producto);
                System.out.println("exito en el dao -------> cantCompCli");
            }
        } catch (Exception e) {
            System.out.println("error en el dao -------> cantCompCli"+e.getMessage());
            e.printStackTrace();
        }
        return cantCompCli;
    }

    @Override
    public List canVenMensualxAnio(int n) throws Exception {
        List canVenMensualxAnio=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_Rep_canVenMensualxAnio(?); ";
            CallableStatement cs=con.prepareCall(sql);
            cs.setInt(1, n);
            ResultSet rs=cs.executeQuery();
            while (rs.next()) {
                canVenMensualxAnio cat=new canVenMensualxAnio();
                cat.setEnero(rs.getInt(1));
                cat.setFebrero(rs.getInt(2));
                cat.setMarzo(rs.getInt(3));
                cat.setAbril(rs.getInt(4));
                cat.setMayo(rs.getInt(5));
                cat.setJunio(rs.getInt(6));
                cat.setJulio(rs.getInt(7));
                cat.setAgosto(rs.getInt(8));
                cat.setSetiembre(rs.getInt(9));
                cat.setOctubre(rs.getInt(10));
                cat.setNoviembre(rs.getInt(11));
                cat.setDiciembre(rs.getInt(12));
                canVenMensualxAnio.add(cat);
                System.out.println("exito en el dao -------> canVenMensualxAnio");
            }
        } catch (Exception e) {
            System.out.println("exito en el dao -------> canVenMensualxAnio"+e.getMessage());
            e.printStackTrace();
        }
        return canVenMensualxAnio;
    }

    @Override
    public List montCatAnual(int n) throws Exception {
        List montCatAnual=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_Rep_montCatAnual(?); ";
            CallableStatement cs=con.prepareCall(sql);
            cs.setInt(1, n);
            ResultSet rs=cs.executeQuery();
            while (rs.next()) {
                montCatAnual cat=new montCatAnual();
                cat.setNomCategoria(rs.getString(1));
                cat.setTotfacturado(rs.getInt(2));
                montCatAnual.add(cat);
                System.out.println("exito en el dao -------> montCatAnual");
            }
        } catch (Exception e) {
            System.out.println("exito en el dao -------> montCatAnual"+e.getMessage());
            e.printStackTrace();
        }
        return montCatAnual;
    }

    @Override
    public List montFacxCliAnual(int n) throws Exception {
        List montFacxCliAnual=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_rep_montFacxCliAnual(?); ";
            CallableStatement cs=con.prepareCall(sql);
            cs.setInt(1, n);
            ResultSet rs=cs.executeQuery();
            while (rs.next()) {
                montFacxCliAnual cat=new montFacxCliAnual();
                cat.setNomCliente(rs.getString(1));
                cat.setMontFacturado(rs.getDouble(2));
                montFacxCliAnual.add(cat);
                System.out.println("exito en el dao -------> montFacxCliAnual");
            }
        } catch (Exception e) {
            System.out.println("exito en el dao -------> montFacxCliAnual"+e.getMessage());
            e.printStackTrace();
        }
        return montFacxCliAnual;
    }

    @Override
    public List montoVendAnual() throws Exception {
        List montoVendAnual=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_Rep_montoVendAnual()";
            CallableStatement cs=con.prepareCall(sql);
            ResultSet rs=cs.executeQuery();
            int cont=0;
            while (rs.next()) {
                cont +=1;
                montoVendAnual producto = new montoVendAnual();
                producto.setAnio(rs.getString(1));
                producto.setMontoVendido(rs.getInt(2));
                montoVendAnual.add(producto);
                System.out.println("exito en el dao -------> montoVendAnual");
            }
        } catch (Exception e) {
            System.out.println("error en el dao -------> montoVendAnual "+e.getMessage());
            e.printStackTrace();
        }
        return montoVendAnual;
    }

    @Override
    public List proTopAnual(int n) throws Exception {
        List proTopAnual=new ArrayList();
        Connection con=null;
        try {
            con=Conexion.getConnection();
            String sql="call sp_rep_proTopAnual(?); ";
            CallableStatement cs=con.prepareCall(sql);
            cs.setInt(1, n);
            ResultSet rs=cs.executeQuery();
            int cont=0;
            while (rs.next()) {
                cont +=1;
                proTopAnual cat=new proTopAnual();
                cat.setNomProd(rs.getString(1));
                cat.setCantidad(rs.getInt(2));
                cat.setPosicion(cont);
                proTopAnual.add(cat);
                System.out.println("exito en el dao -------> proTopAnual");
                //perfecto para las barras
            }
        } catch (Exception e) {
            System.out.println("exito en el dao -------> proTopAnual"+e.getMessage());
            e.printStackTrace();
        }
        return proTopAnual;
    }

    @Override
    public List proTopFechas(FechaIF fechaif) throws Exception {
        List cantCompCli=new ArrayList();
        Connection cn = null;
        CallableStatement cstm = null;
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_rep_proTopFechas(?,?);";
            cstm = cn.prepareCall(Comando);
            cstm.setDate(1, fechaif.getInicio());
            cstm.setDate(2, fechaif.getFin());
            ResultSet rs = cstm.executeQuery();
            while (rs.next()) {
                FechaIF resultado =new FechaIF();
                resultado.setNomCate(rs.getString(1));
                resultado.setCantidad(rs.getInt(2));
                cantCompCli.add(resultado);
            }
            System.out.println(" exito dao -------> proTopFechas ");
        }catch(Exception e){
            System.out.println(" error dao -------> proTopFechas  "+e.getMessage());
        }
        return cantCompCli;
    }

    @Override
    public List proMasVendCat(FechaIF fechaif) throws Exception {
        List proMasVendCat=new ArrayList();
        Connection cn = null;
        CallableStatement cstm = null;
        try{
            cn = Conexion.getConnection();
            String Comando = "call sp_rep_proMasVendCat(?,?,?);";
            cstm = cn.prepareCall(Comando);
            cstm.setInt(1, fechaif.getCategoria().getIdCategoria());
            cstm.setDate(2, fechaif.getInicio());
            cstm.setDate(3, fechaif.getFin());
            ResultSet rs = cstm.executeQuery();
            while (rs.next()) {
                FechaIF resultado =new FechaIF();
                resultado.setNomCate(rs.getString(1));
                resultado.setCantidad(rs.getInt(2));
                proMasVendCat.add(resultado);
            }
            System.out.println(" exito dao -------> proTopFechas ");
        }catch(Exception e){
            System.out.println(" error dao -------> proTopFechas  "+e.getMessage());
        }
        return proMasVendCat;
    }

    @Override
    public List cantVentCliMesAnio(int a, int b) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}