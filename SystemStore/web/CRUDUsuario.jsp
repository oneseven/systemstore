<%-- 
    Document   : CRUDUsuario
    Created on : 30-nov-2013, 15:06:10
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <s:actionerror theme="bootstrap"/>
        <s:actionmessage theme="bootstrap"/>
        <s:fielderror theme="bootstrap"/>
        <s:url id="remoteurl" action="ListarUsuariosXML"></s:url>
    <sjg:grid
        id="gridtable" caption="Usuarios" dataType="json" href="%{remoteurl}"
        pager="true" gridModel="gridModel" rowList="10,15,20" rowNum="10"
        navigator="true" navigatorAddOptions="{height:280,reloadAfterSubmit:true}"
        rownumbers="true" autowidth="true">
        
        <sjq:gridColumn name="idUsuario" index="idUsuario" title="Codigo Usuario"
                        sortable="false" align="center" editable="true" edittype="text" />
        <sjq:gridColumn name="tipo.nombreTipo" index="tipo.nombreTipo" title="Tipo Usuario"
                        sortable="false" align="center" editable="true" edittype="select"
                        editoptions="{value:'ADMINISTRADOR:ADMINISTRADOR;USUARIO:USUARIO'}"/>
        <sjq:gridColumn name="usuario" index="usuario" title="Usuario"
                        sortable="false" align="center" editable="true" edittype="text" />
        <sjq:gridColumn name="nomUsuario" index="nomUsuario" title="Nombre"
                        sortable="false" align="center" editable="true" edittype="text" />
        <sjq:gridColumn name="apeUsuario" index="apeUsuario" title="Apellido"
                        sortable="false" align="center" editable="true" edittype="text" />
    </sjg:grid>
    </body>
</html>
