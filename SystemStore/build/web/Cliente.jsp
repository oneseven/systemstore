<%-- 
    Document   : Catalogo
    Created on : 17-nov-2013, 15:06:04
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <sb:head/>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="Bootstrap/img/logo.png" alt="Logo"/>
            </a>
           
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <s:form action="updateDatosClienteXML"  theme="bootstrap" 
                cssClass="form-horizontal" label="Datos Personales">
            
        <s:textfield label="Nombre" name="nomCli" tooltip="Ingrese su nombre" value="%{Cliente.nombre}"/>
        <s:textfield label="Apellidos" name="apeCli" tooltip="Ingrese sus apellidos" value="%{Cliente.apellidos}" />
        <s:textfield label=" DNI " name="dni" tooltip="Ingrese su DNI" value="%{Cliente.dni}" />
        <s:textfield label="Celular" name="celCli" tooltip="Ingrese su numero movil" value="%{Cliente.celular}" />
        <s:textfield label="Correo" name="emailCli" tooltip="Ingrese su telefono" value="%{Cliente.telefono}" />
        
        <s:textfield label="Fecha Nacimiento" name="fechNacCli" tooltip="Fecha en la que nacio" value="%{Cliente.fecNacimniento}" />
        <s:textfield label="Direccion" name="dirCli" tooltip="Direccion de casa" value="%{Cliente.direccion}" />
        
        
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <s:submit cssClass="btn btn-success" value="Actualizar Datos"/>
        </s:form>
    </body>
</html>
