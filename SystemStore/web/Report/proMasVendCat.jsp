<%-- 
    Document   : proMasVendCat
    Created on : 02-dic-2013, 19:19:47
    Author     : Julio Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="ch" uri="/struts-jquery-chart-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <link href="../Bootstrap/icomoon/style.css" rel="stylesheet">
        <link href="../Bootstrap/css/main.css" rel="stylesheet">
        <sj:head jqueryui="true" jquerytheme="blitzer"></sj:head>
        <title>Reporte de Ventas</title>
    </head>
    <body>
        <header>
            <a href="#" class="logo">
                <img src="../Bootstrap/img/logo.png" alt="Logo"/>
            </a>
        </header>
        <div class="container-fluid">
          <div class="dashboard-container">
            <div class="top-nav">
              <ul>
              <s:iterator value="listarPaginas">
                <li>
                    <sj:menuItem  title="%{Titulo}" href="%{Url}"></sj:menuItem>
                </li>
                </s:iterator>
              </ul>
              <div class="clearfix">
              </div>
            </div>
          </div>
        </div>
        <h1>Producto mas vendido por categoria</h1>
        <center><br><br>
            <s:form action="proMasVenCatXML">
                <sj:autocompleter label="Categoria: "
                              name="idCat"
                              list="categorias"
                              listKey="idCategoria"
                              listValue="nomCategoria" 
                              parentTheme="bootstrap"
                              tooltip="Tooltip for Autocompleter"/>
                <sj:datepicker name="inicio" displayFormat="dd/mm/yy" label="Inicio" /><br>
                <sj:datepicker name="fin" displayFormat="dd/mm/yy" label="Fin" />
                <s:submit value="enviar"></s:submit>
            </s:form>
            <div id="cont">
            <h2>proTopFechas</h2>     
            <ch:chart id="chartPie"  cssStyle="width: 600px; height: 400px;" autoResize="true" pie="true" pieLabel="true" >
               <s:iterator value="proTopFechas">
                   <ch:chartData id="chardata1" label="%{nomPro}"  data="%{cantidad}" />
               </s:iterator>
            </ch:chart>
            </div>
        </center>
    <a href="Reportes.jsp"><div id="btn">Regresar</div></a>
    </body>
</html>

